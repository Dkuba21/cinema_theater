### Intro

Hello, my name is Dmitriy. I'm a Python developer, and I'm looking for a new job opportunities. Employers often
ask applicants to show some examples of code. But what if all code I write is 
proprietary, and I'm not a contributor of Django (Flask etc.)?

That's why I decided to create something which could reflect my job skills. That's how 
cinema theater API services project was born.

The project is based on follow technologies mostly:
- Django (for inner admin panel only)
- Postgresql
- Redis
- Elasticsearch
- Fastapi
- Flask
- Pytest

I built my project using technologies and approaches I use on daily base at my work.

Please, consider the project isn't finished yet and its under construction. 
There is even no manual how to run services all together (please see devops folder),
but there is a code you can see, and we could talk about. 
The project will be finalized soon. My e-mail: ```dkubarev88@gmail.com```

### TODO list:

- load test data while admin_panel service is starting
- create common makefile for all services build and run
- translate all docstrings and comments to English
- add main diagram to show how all services work all together