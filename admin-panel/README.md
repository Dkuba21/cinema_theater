# Movie admin panel

The project implements online cinema theater admin panel. The service allows managing database (CRUD) and keep movies info in order

## Using technologies 

- Django framework is used
- Service works under gunicorn web server
- Static files are served by Nginx
- Virtualization is based on **Docker.**

## Install

- Prepare environment using .env file on the base of .env_example file 


- Build and run docker images  
```docker-compose up -d --build``` 


- Apply Django migrations  
```docker-compose exec web python manage.py migrate --noinput``` 


- Create Django superuser  
```docker-compose exec web python manage.py createsuperuser```

- Check if admin panel is available  
``http:\\localhost:{APP_PORT}\admin``