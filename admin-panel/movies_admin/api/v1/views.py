from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import Q
from django.http import JsonResponse
from django.views.generic.detail import BaseDetailView
from django.views.generic.list import BaseListView

from movies.models import FilmWork, PersonRoleType


class MoviesApiMixin:
    model = FilmWork
    http_method_names = ['get']

    def get_queryset(self):
        relevant_fields = FilmWork.fields()
        relevant_fields.remove('created_at')
        relevant_fields.remove('updated_at')
        movies = FilmWork.objects.prefetch_related('genres', 'persons').values(*relevant_fields).annotate(
            genres=ArrayAgg('genres__name', distinct=True)).annotate(
            actors=ArrayAgg('persons__full_name', distinct=True,
                            filter=Q(filmworkperson__role=PersonRoleType.ACTOR))).annotate(
            directors=ArrayAgg('persons__full_name', distinct=True,
                               filter=Q(filmworkperson__role=PersonRoleType.DIRECTOR))).annotate(
            writers=ArrayAgg('persons__full_name', distinct=True,
                             filter=Q(filmworkperson__role=PersonRoleType.WRITER))).order_by('title')
        return movies

    def render_to_response(self, context, **response_kwargs):
        return JsonResponse(context)


class MoviesListApi(MoviesApiMixin, BaseListView):
    paginate_by = 50

    def get_context_data(self, *, object_list=None, **kwargs):
        paginator, page, queryset, is_paginated = self.paginate_queryset(
            self.get_queryset(),
            self.paginate_by
        )

        context = {
            'count': paginator.count,
            "total_pages": paginator.num_pages,
            'prev': page.previous_page_number() if page.has_previous() else None,
            'next': page.next_page_number() if page.has_next() else None,
            'results': list(queryset),
        }
        return context


class MoviesDetailApi(MoviesApiMixin, BaseDetailView):

    def get_context_data(self, **kwargs):
        return kwargs['object']
