from django.contrib import admin

from .models import FilmWork, Person, Genre, FilmWorkPerson, FilmWorkGenre


class FilmWorkPersonInline(admin.TabularInline):
    model = FilmWorkPerson
    exclude = ('id',)


class FilmWorkGenreInline(admin.TabularInline):
    model = FilmWorkGenre
    exclude = ('id',)


@admin.register(FilmWork)
class FilmWorkAdmin(admin.ModelAdmin):
    inlines = [FilmWorkPersonInline, FilmWorkGenreInline]
    list_display = ('title', 'type', 'creation_date', 'rating')

    search_fields = ('title', 'description', 'id', 'type', 'rating')

    fields = (
        'title', 'type', 'description', 'creation_date', 'certificate',
        'file_path', 'rating'
    )


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    pass


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    search_fields = ('full_name',)
