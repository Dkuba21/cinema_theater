import uuid

from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import MinValueValidator


class TimeStampedMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Genre(TimeStampedMixin):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(_('title'), max_length=255)
    description = models.TextField(_('description'), blank=True, null=True)

    class Meta:
        verbose_name = _('genre')
        verbose_name_plural = _('genres')
        db_table = '"content"."genre"'

    def __str__(self):
        return self.name


class Person(TimeStampedMixin):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    full_name = models.CharField(_('title'), max_length=255, db_column='full_name')
    birth_date = models.DateField(_('description'), blank=True, null=True, db_column='birth_date')

    class Meta:
        verbose_name = _('person')
        verbose_name_plural = _('persons')
        db_table = '"content"."person"'

    def __str__(self):
        return self.full_name


class PersonRoleType(models.TextChoices):
    DIRECTOR = 'director', _('director')
    ACTOR = 'actor', _('actor')
    WRITER = 'writer', _('writer')
    PRODUCER = 'producer', _('producer')


class FilmWorkPerson(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    film_work = models.ForeignKey('FilmWork', on_delete=models.CASCADE,
                                  verbose_name=_('film_work'))
    person = models.ForeignKey('Person', on_delete=models.CASCADE,
                               verbose_name=_('person'))
    role = models.CharField(_('role'), max_length=50, choices=PersonRoleType.choices)
    created_at = models.DateTimeField(auto_now_add=True, db_column='created_at')

    class Meta:
        db_table = '"content"."person_film_work"'
        index_together = ['film_work_id', 'person_id', 'role']
        unique_together = ['film_work_id', 'person_id', 'role']


class FilmWorkGenre(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    film_work = models.ForeignKey('FilmWork', on_delete=models.CASCADE,
                                  verbose_name=_('film_work'))
    genre = models.ForeignKey('Genre', on_delete=models.CASCADE,
                              verbose_name=_('genre'))
    created_at = models.DateTimeField(auto_now_add=True, db_column='created_at')

    class Meta:
        db_table = '"content"."genre_film_work"'
        index_together = ['film_work_id', 'genre_id']
        unique_together = ['film_work_id', 'genre_id']


class FilmWorkType(models.TextChoices):
    MOVIE = 'movie', _('movie')
    TV_SHOW = 'tv_show', _('TV Show')


class FilmWork(TimeStampedMixin):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    title = models.CharField(_('title'), max_length=255, db_index=True)
    description = models.TextField(_('description'), blank=True, null=True)
    creation_date = models.DateField(_('creation date'), blank=True, null=True, db_column='creation_date',
                                     db_index=True)
    certificate = models.TextField(_('certificate'), blank=True, null=True, db_index=True)
    file_path = models.FileField(_('file'), upload_to='film_works/', blank=True, null=True, db_column='file_path')
    rating = models.FloatField(_('rating'), validators=[MinValueValidator(0)], blank=True, null=True, db_index=True)
    type = models.CharField(_('type'), max_length=20, choices=FilmWorkType.choices)
    genres = models.ManyToManyField(Genre, through='FilmWorkGenre')
    persons = models.ManyToManyField(Person, through='FilmWorkPerson')

    class Meta:
        verbose_name = _('filmwork')
        verbose_name_plural = _('filmworks')
        db_table = '"content"."film_work"'
        ordering = ['title']

    def __str__(self):
        return self.title

    @classmethod
    def fields(cls):
        return [f.name for f in cls._meta.fields]
