## Deploying the app for development

```
git clone git@github.com:dkuba/Auth_sprint_2.git

cd Auth_sprint_2
make dev/setup

# run intergation tests 
make test/run_integration

# available options
make help
```

## Docs

Swagger UI docs is available on ```/apidocs/``` endpoint