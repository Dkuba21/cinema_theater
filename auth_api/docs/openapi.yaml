openapi: 3.0.1
info:
  title: Auth API
  description: Authorization service
  termsOfService: ""
  contact:
    email: dkubarev2517@gmail.com
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
servers:
- url: http://localhost:5000/api/v1
tags:
- name: public
  description: Public interface
- name: me
  description: Authorized user work with their own data
- name: role
  description: Roles control
- name: user_role
  description: User-Role ationship
paths:
  /login:
    post:
      tags:
      - public
      summary: user login
      description: User authentification
      requestBody:
        content:
          application/json:
            schema:
              required:
              - login
              - password
              type: object
              properties:
                login:
                  type: string
                password:
                  type: string
        required: true
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TokenPair'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
  /user:
    post:
      tags:
      - public
      summary: Create user
      description: Create new user in the system
      requestBody:
        description: user info object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InputCreateUser'
      responses:
        201:
          description: new user has been created successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        409:
          $ref: '#/components/responses/409_Conflict'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
  /me:
    put:
      tags:
      - me
      summary: Update data
      description: update existing user data
      requestBody:
        description: user info object
        required: true
        content:
          applicatioin/json:
            schema:
              $ref: '#/components/schemas/InputUpdateUser'
      responses:
        200:
          description: Existing user updated successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        404:
          $ref: '#/components/responses/404_NotFound'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
      security:
      - bearerAuth: []
  /me/logout:
    get:
      tags:
      - me
      summary: Logout
      description: logout user and close the session
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EmptyObject'
        401:
          $ref: '#/components/responses/401_Unauthorized'
      security:
      - bearerAuth: []
  /me/logout_other_devices:
    get:
      tags:
      - me
      summary: Logout from other accounts
      description: logout user from other sessions except current
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EmptyObject'
        401:
          $ref: '#/components/responses/401_Unauthorized'
      security:
      - bearerAuth: []
  /me/refresh_token:
    put:
      tags:
      - public
      summary: Update access token
      description: update user access token
      requestBody:
        content:
          application/json:
            schema:
              required:
              - refresh_token
              type: object
              properties:
                refresh_token:
                  type: string
                  description: Long lifetime refresh token
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TokenPair'
        401:
          $ref: '#/components/responses/401_Unauthorized'
        404:
          $ref: '#/components/responses/404_NotFound'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
  /me/access_history:
    get:
      tags:
      - me
      summary: See user login history
      description: allow to see user user login history for all sessions have been commited
      responses:
        200:
          description: user login history data
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListOfAccessHistory'
        401:
          $ref: '#/components/responses/401_Unauthorized'
      security:
      - bearerAuth: []
  /role:
    get:
      tags:
      - role
      summary: Role list
      description: All roles in the sustem
      responses:
        200:
          description: role list
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListOfRoles'
        401:
          $ref: '#/components/responses/401_Unauthorized'
        403:
          $ref: '#/components/responses/403_Forbidden'
      security:
      - bearerAuth: []
    post:
      tags:
      - role
      summary: Create role
      requestBody:
        description: role info object
        required: true
        content:
          applicatioin/json:
            schema:
              $ref: '#/components/schemas/InputCreateRole'
      responses:
        201:
          description: new role created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Role'
        409:
          $ref: '#/components/responses/409_Conflict'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
      security:
      - bearerAuth: []
  /role/{roleId}:
    get:
      tags:
      - role
      summary: Get one role by id
      parameters:
        - in: path
          name: roleId
          schema:
            type: string
            format: uuid
          required: true
          description: role id
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Role'
        401:
          $ref: '#/components/responses/401_Unauthorized'
        403:
          $ref: '#/components/responses/403_Forbidden'
        404:
          $ref: '#/components/responses/404_NotFound'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
      security:
      - bearerAuth: []
    put:
      tags:
      - role
      summary: Update role
      parameters:
        - in: path
          name: roleId
          schema:
            type: string
            format: uuid
          required: true
          description: role id
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InputUpdateRole'
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Role'
        401:
          $ref: '#/components/responses/401_Unauthorized'
        403:
          $ref: '#/components/responses/403_Forbidden'
        404:
          $ref: '#/components/responses/404_NotFound'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
      security:
      - bearerAuth: []
    delete:
      tags:
      - role
      summary: Delete role from the system
      parameters:
        - in: path
          name: roleId
          schema:
            type: string
            format: uuid
          required: true
          description: role id
      responses:
        204:
          description: role removed successfully
        401:
          $ref: '#/components/responses/401_Unauthorized'
        403:
          $ref: '#/components/responses/403_Forbidden'
        404:
          $ref: '#/components/responses/404_NotFound'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
      security:
      - bearerAuth: []
      
  /user/{userId}/role/{roleId}:
    get:
      tags:
      - user_role
      summary: Check if user has a role
      parameters:
        - in: path
          name: userId
          schema:
            type: string
            format: uuid
          required: true
          description: user id
        - in: path
          name: roleId
          schema:
            type: string
            format: uuid
          required: true
          description: role id
      responses:
        200:
          description: successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/HasRole'
        401:
          $ref: '#/components/responses/401_Unauthorized'
        403:
          $ref: '#/components/responses/403_Forbidden'
        409:
          $ref: '#/components/responses/409_Conflict'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
      security:
      - bearerAuth: []
    post:
      tags:
      - user_role
      summary: Assign role for the user
      parameters:
        - in: path
          name: userId
          schema:
            type: string
            format: uuid
          required: true
          description: user id
        - in: path
          name: roleId
          schema:
            type: string
            format: uuid
          required: true
          description: role id
      responses:
        201:
          description: role assigned successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EmptyObject'
        401:
          $ref: '#/components/responses/401_Unauthorized'
        403:
          $ref: '#/components/responses/403_Forbidden'
        409:
          $ref: '#/components/responses/409_Conflict'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
      security:
      - bearerAuth: []
    delete:
      tags:
      - user_role
      summary: Delete role from user
      parameters:
        - in: path
          name: userId
          schema:
            type: string
            format: uuid
          required: true
          description: user id
        - in: path
          name: roleId
          schema:
            type: string
            format: uuid
          required: true
          description: role id
      responses:
        204:
          description: role deleted successfully
        401:
          $ref: '#/components/responses/401_Unauthorized'
        403:
          $ref: '#/components/responses/403_Forbidden'
        404:
          $ref: '#/components/responses/404_NotFound'
        422:
          $ref: '#/components/responses/422_UnprocessableEntity'
      security:
      - bearerAuth: []
components:
  responses:
    401_Unauthorized:
      description: User authentification failed. Access token is missing or wrong
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    403_Forbidden:
      description: Access denied. User needs elevation of rights
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    404_NotFound:
      description: Resource with such id is not exists
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    409_Conflict:
      description: Resource with such id is already exists
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    422_UnprocessableEntity:
      description: Validation error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    default:
      description: Unpredictable error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
  schemas:
    UserIDBase:
      type: object
      required:
        - id
      properties:
        id:
          type: string
          format: uuid
          description: user id (in UUID format)
    UserBase:
      type: object
      properties:
        username:
          type: string
          description: user name
        email:
          type: string
          description: email
        password:
          type: string
          description: user password
    InputCreateUser:
      allOf:
        - $ref: '#/components/schemas/UserBase'
        - type: object
          required:
          - username
          - email
          - password
    InputUpdateUser:
      allOf:
        - $ref: '#/components/schemas/UserBase'
        - $ref: '#/components/schemas/UserIDBase'
    User:
      allOf:
        - $ref: '#/components/schemas/UserBase'
        - $ref: '#/components/schemas/UserIDBase'
        - type: object
          required:
          - username
          - email
          - password
    TokenPair:
      required:
      - "access"
      - "refresh"
      type: object
      properties:
        access:
          type: string
          description: short lifetime access token
        refresh:
          type: string
          description: long liftime refresh token
      xml:
        name: Order
    Error:
      type: object
      required:
      - "message"
      properties:
        message:
          type: string
          description: Server error message
    EmptyObject:
      type: object
    AccessHistory:
      type: object
      properties:
        user_agent:
          type: string
          description: user agent (device, browser etc.) info
        datetime:
          type: string
          format: date-time
          description: login date
    ListOfAccessHistory:
      type: array
      items:
        $ref: '#/components/schemas/AccessHistory'
    HasRole:
      type: object
      required:
      - has_role
      properties:
        has_role:
          type: boolean
          description: user has a role flag
    Role:
      type: object
      required:
      - id
      - name
      properties:
        id:
          type: string
          format: uuid
          description: role id
        name:
          type: string
          description: role name
    InputCreateRole:
      type: object
      required:
      - name
      properties:
        name:
          type: string
          description: role name
    InputUpdateRole:
      $ref: '#/components/schemas/InputCreateRole'
    ListOfRoles:
      type: array
      items:
        $ref: '#/components/schemas/Role'
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT  
