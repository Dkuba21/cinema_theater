from functools import wraps

from flask import request

import exceptions as exc
from config import Config
from models.api.tokens import AccessToken
from services import JWTService, get_jwt_service


def auth_required(f):
    """
    Decorator is used in controllers for authorization.

    It extracts the access token from the header (Header format:
    "Authorization: Bearer <token_string>"), decrypts and validates it.
    In decorated function you can get the decoded token by named "access_token".

    If there is no token or it is not valid, the decorator returns
    various exceptions. Exceptions are caught at the level
    Flask framework and form a response understandable to the Client.
    """
    jwt_service: JWTService = get_jwt_service()

    @wraps(f)
    def decorator(*args, **kwargs):
        access_token_str = None

        if "Authorization" in request.headers:
            token_header = request.headers["Authorization"].split(" ")
            access_token_str = token_header[-1]

        if not access_token_str:
            raise exc.ApiForbiddenUserException

        access_token: AccessToken = jwt_service.decode(encoded_token=access_token_str)

        if access_token.type != Config.ACCESS_TOKEN_TYPE:
            raise exc.ApiTokenWrongTypeException

        jwt_service.is_expired(token=access_token)
        jwt_service.is_in_blacklist(token=access_token)

        return f(*args, access_token, **kwargs)

    return decorator
