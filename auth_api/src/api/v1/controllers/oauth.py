from flask import url_for, session, request

from models.api.user import OAuthUser

from http import HTTPStatus as status

from services import (get_oauth_service, UserService,
                      get_user_service, get_jwt_service,
                      JWTService)


class OAuthController:
    def __init__(
            self,
            oauth_service=get_oauth_service(),
            user_service: UserService = get_user_service(),
            jwt_service: JWTService = get_jwt_service(),

    ):
        self.jwt_service = jwt_service
        self.oauth_service = oauth_service
        self.user_service = user_service

    def oauth_login(self, provider: str):
        """Authorization with third-party OAuth provider."""
        oauth_service = self.oauth_service.get_provider(provider)
        redirect_uri = url_for(f'api.v1.oauth_callback', provider=provider,
                               _external=True)
        session["User-Agent"] = request.headers["User-Agent"]
        return oauth_service.authorize(redirect_uri)

    def oauth_callback(self, provider):
        """
        OAuth provider callback function.

        a) get (create if not exist) social user account in database.
        b) get related user.
        c) user login
            - create token pair for logged-in user
            - add access history entry
            - store refresh token to database
            - return token pairs
        """
        oauth = self.oauth_service.get_provider(provider)
        social_user: OAuthUser = oauth.callback()

        user = self.user_service.get_or_create_social_user(social_user)

        token_pair = self.jwt_service.create_token_pair(user=user)

        self.user_service.create_access_history(user,
                                                dict(session).get("User-Agent", None))
        self.jwt_service.store_refresh_token(token_pair.refresh)

        return {
                   "access": token_pair.access.encoded_token,
                   "refresh": token_pair.refresh.encoded_token,
               }, status.OK
