"""Controller serves user-role relationship."""
import logging
from uuid import UUID

from flask import jsonify, request

from http import HTTPStatus as status

import exceptions as exc
from api.helpers import auth_required
from models.api.tokens import AccessToken
from models.api.user import InputUserRole, InputUpdateUser, OutputUser, User
from services import UserService, get_user_service


class UserController:
    def __init__(self, user_service: UserService = get_user_service()):
        self.user_service = user_service

    @auth_required
    def set_role_to_user(self, access_token: AccessToken, user_id: UUID, role_id: UUID):
        """Set role to user."""

        InputUserRole(user_id=user_id, role_id=role_id)

        self.user_service.set_role_to_user(user_id=user_id, role_id=role_id)

        return jsonify({}), status.CREATED

    @auth_required
    def check_user_role(self, access_token: AccessToken, user_id: UUID, role_id: UUID):
        """Check if user has a role."""

        # валидация входных параметров
        InputUserRole(user_id=user_id, role_id=role_id)

        has_role = self.user_service.check_user_role(user_id=user_id, role_id=role_id)
        if has_role:
            response_data = {"has_role": True}
        else:
            response_data = {"has_role": False}

        return jsonify(response_data), status.OK

    @auth_required
    def delete_user_role(self, access_token: AccessToken, user_id: UUID, role_id: UUID):
        """Remove role from user."""

        # валидация входных параметров
        InputUserRole(user_id=user_id, role_id=role_id)

        self.user_service.delete_user_role(user_id=user_id, role_id=role_id)

        return {}, status.NO_CONTENT

    @auth_required
    def update_current_user(self, access_token: AccessToken):
        """Update user credentials."""

        user = InputUpdateUser.parse_obj(request.json)

        if str(user.id) != str(access_token.user_id):
            logging.error(f"Request user: {user.id} Token user: {access_token.user_id}")
            raise exc.ApiForbiddenUserException(
                detail="It's prohibited to change other user credentials."
            )

        updated_user: User = self.user_service.update_user(user)

        output_user = OutputUser(**updated_user.dict())

        return jsonify(output_user.dict()), status.OK
