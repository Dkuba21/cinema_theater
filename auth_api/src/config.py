import os
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()


class Config(object):
    """App configuration."""

    BASE_DIR = Path(__file__).parent
    TESTING = False
    DEBUG = False

    SWAGGER = {
        'title': 'Flasgger Parsed Method/Function View Example',
        'doc_dir': '../docs/',
        'openapi': '3.0.1'
    }

    POSTGRES_HOST = os.environ.get("POSTGRES_HOST")
    POSTGRES_DB = os.environ.get("POSTGRES_DB")
    POSTGRES_USER = os.environ.get("POSTGRES_USER")
    POSTGRES_PASSWORD = os.environ.get("POSTGRES_PASSWORD")

    OAUTH_CREDENTIALS = {
        'google': {
            'id': os.environ.get("GOOGLE_ID"),
            'secret': os.environ.get('GOOGLE_SECRET')
        },
    }

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return (
            f"postgresql://{self.POSTGRES_USER}:{self.POSTGRES_PASSWORD}"
            f"@{self.POSTGRES_HOST}/{self.POSTGRES_DB}"
        )

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SECRET_KEY = os.environ.get("SECRET_KEY")
    JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY")

    JSON_AS_ASCII = False

    # access token lifitime in minutes
    ACCESS_TOKEN_LIFETIME = 10

    # refresh token lifetime in minutes
    REFRESH_TOKEN_LIFETIME = 14400

    # encoding algo for jwt
    JWT_ALGORITHM = "HS256"

    # dat time pattern for jwt tokens
    JWT_DATETIME_PATTERN = "%Y-%m-%d %H:%M:%S"

    REDIS_HOST = os.environ.get("REDIS_HOST")
    REDIS_PORT = os.environ.get("REDIS_PORT")
    REDIS_DB = 0

    @property
    def REDIS_URL(self):
        return f"redis://{self.REDIS_HOST}:{self.REDIS_PORT}"

    ACCESS_TOKEN_TYPE = "access"
    REFRESH_TOKEN_TYPE = "refresh"

    # available user roles
    ADMIN_ROLE_NAME = "admin"
    USER_ROLE_NAME = "user"
    SUBSCRIBER_ROLE_NAME = "subscriber"

    POSSIBLE_ROLE_NAMES = [ADMIN_ROLE_NAME, USER_ROLE_NAME, SUBSCRIBER_ROLE_NAME]


class ProductionConfig(Config):
    """Production only config."""


class TestingConfig(Config):
    """Test only config."""

    TESTING = True
    POSTGRES_DB = os.environ.get("POSTGRES_DB_TEST")
    REDIS_DB = 1


class DevelopmentConfig(Config):
    """Development only config."""

    DEBUG = True


environment = os.environ.get("FLASK_ENV")

if environment == "production":
    app_config = ProductionConfig()
elif environment == "testing":
    app_config = TestingConfig()
else:
    app_config = DevelopmentConfig()
