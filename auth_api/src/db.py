from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()


def init_db(app: Flask) -> SQLAlchemy:
    """Database connection."""
    db.init_app(app)

    return db


def init_migrate(app: Flask, db: SQLAlchemy) -> Migrate:
    """Database migration applying."""
    migrate.init_app(app, db)

    return migrate
