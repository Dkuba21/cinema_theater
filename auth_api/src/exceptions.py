"""All custom exception."""

from typing import Optional


class ApiException(Exception):
    """Base exception."""

    http_status_code = 500
    message = "Generic error"

    def __init__(
        self,
        message: Optional[str] = None,
        detail: Optional[str] = None,
        payload: Optional[dict] = None,
    ):
        super().__init__()

        if message:
            self.message = message
        else:
            self.message = self.message

        self.detail = detail
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        rv["detail"] = self.detail
        return rv


class ApiInvalidParamsException(ApiException):
    http_status_code = 422


class ApiConflictException(ApiException):
    http_status_code = 409


class ApiNotFoundException(ApiException):
    http_status_code = 404


class ApiForbiddenException(ApiException):
    http_status_code = 403


class ApiUnauthorizedException(ApiException):
    http_status_code = 401


#
# Authorization
#


class ApiValidationErrorException(ApiInvalidParamsException):
    message = "Validation error"


class ApiLoginInvalidParamsException(ApiInvalidParamsException):
    message = "Wrong username or password"


#
# User registration
#


class ApiLoginInUseException(ApiConflictException):
    message = "Username is in use already"


class ApiEmailInUseException(ApiConflictException):
    message = "Email is in use already"


class ApiUserAlreadyExistsException(ApiConflictException):
    message = "User with same username or e-mail is already exists"


#
# Access token renovation
#


class ApiTokenValidationException(ApiUnauthorizedException):
    message = "Token is not valid"


class ApiTokenNotFoundException(ApiNotFoundException):
    message = "Token is not found"


class ApiTokenWrongTypeException(ApiUnauthorizedException):
    message = "Wrong token type"


#
# User
#


class ApiUserNotFoundException(ApiNotFoundException):
    message = "User is not found"


class ApiUserValidationException(ApiInvalidParamsException):
    message = "User validation error"


class ApiForbiddenUserException(ApiForbiddenException):
    message = "Access forbidden"


#
# Roles
#


class ApiRoleNotFoundException(ApiNotFoundException):
    message = "Role is not found"


class ApiRoleAlreadyExistsException(ApiConflictException):
    message = "Role is already exists"


class ApiRoleValidationException(ApiInvalidParamsException):
    message = "Role validation error"


#
# User-Role
#


class ApiUserRoleValidationException(ApiInvalidParamsException):
    message = "Invalid user or role"


#
# OAuth
#

class ApiUserAuthenticationFailedTypeException(ApiUnauthorizedException):
    message = "Authentication error"
