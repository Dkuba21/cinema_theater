from models.api.tokens import RefreshToken, TokenPair
from services.jwt_service import JWTService
from storage import IBlackListStorage
from storage.jwt_storage import IJwtStorage
from storage.user_storage import IUserStorage


class AuthService:
    """

    Authorization service.

    AuthService responsible for authorization routines.
    """

    def __init__(
        self,
        user_storage: IUserStorage,
        jwt_storage: IJwtStorage,
        jwt_service: JWTService,
        black_list_storage: IBlackListStorage,
    ):

        self.user_storage = user_storage
        self.jwt_storage = jwt_storage
        self.jwt_service = jwt_service
        self.black_list_storage = black_list_storage

    def refresh_token(self, encoded_refresh_token: str) -> TokenPair:
        """Get new token pair by refresh token.

        Scenario:
         - decode refresh token
         - check refresh token lifetime
         - validate refresh token and check that it hasn't been used before for
         getting new pair (just check database entry by refresh token jti)
         - produce new token pair (access + refresh)
         - store pair jti in database
         - remove old refresh token entry in database
         - put access token to the blacklist (Redis)
         - return token pair json
        """

        refresh_token: RefreshToken = self.jwt_service.decode(
            encoded_token=encoded_refresh_token
        )

        self.jwt_service.is_refresh_token(refresh_token)

        self.jwt_service.is_expired(token=refresh_token)

        self.jwt_storage.get_refresh_token(jti=refresh_token.jti)

        user = self.user_storage.get_user(refresh_token.user_id)

        token_pair: TokenPair = self.jwt_service.create_token_pair(user=user)

        self.jwt_storage.store_refresh_token(
            jti=token_pair.jti, user_id=user.id, expire_time=token_pair.refresh.expired
        )

        self.jwt_storage.remove_refresh_token(jti=refresh_token.jti)

        self.black_list_storage.set_data(jti=str(refresh_token.jti))

        return token_pair
