import abc

from flask import url_for

from models.api.user import OAuthUser
from oauth import oauth
from config import app_config


class OAuthService(object):
    """Base class to implement and get any available OAuth2 provider class."""
    providers = None

    def __init__(self, provider_name):
        self.provider_name = provider_name
        credentials = app_config.OAUTH_CREDENTIALS[provider_name]
        self.consumer_id = credentials['id']
        self.consumer_secret = credentials['secret']

    def authorize(self, redirect_uri):
        """
        Call to start authorization on provider side.

        :param redirect_uri: uri which oauth provider call when authorization
        is completed on its side
        """
        client = oauth.create_client('google')
        return client.authorize_redirect(redirect_uri)

    @abc.abstractmethod
    def callback(self) -> OAuthUser:
        """
        Callback url handler.

        Callback url should be registered on OAuth provider side
        (Google cloud api panel, etc.).
        """
        pass

    def get_callback_url(self):
        return url_for('oauth_callback', provider=self.provider_name,
                       _external=True)

    @classmethod
    def get_provider(cls, provider_name):
        if cls.providers is None:
            cls.providers = {}
            for provider_class in cls.__subclasses__():
                provider = provider_class()
                cls.providers[provider.provider_name] = provider
        return cls.providers[provider_name]


class GoogleService(OAuthService):
    def __init__(self):
        super(GoogleService, self).__init__('google')
        self.service = oauth.register(
                name='google',
                client_id=self.consumer_id,
                client_secret=self.consumer_secret,
                access_token_url='https://accounts.google.com/o/oauth2/token',
                access_token_params=None,
                authorize_url='https://accounts.google.com/o/oauth2/auth',
                authorize_params=None,
                api_base_url='https://www.googleapis.com/oauth2/v1/',
                client_kwargs={'scope': 'openid profile email'},
            )

    def callback(self) -> OAuthUser:
        google = oauth.create_client('google')
        google.authorize_access_token()

        resp = google.get('userinfo')
        user_info = resp.json()

        social_user = OAuthUser(id=user_info['id'], username=user_info['name'],
                                email=user_info['email'], provider_name='google')

        return social_user
