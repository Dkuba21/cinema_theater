from uuid import UUID

import exceptions as exc
from config import app_config
from models.api.user import AccessHistory, InputCreateUser, InputUpdateUser, User, \
    OAuthUser, SocialAccount
from storage import IUserStorage
from storage.role_storage import IRoleStorage
from utils.password import password_hash


class UserService:
    """
    User service class.

    It works with user entity.
    """

    def __init__(
        self,
        user_storage: IUserStorage,
        role_storage: IRoleStorage,
    ):
        self.user_storage = user_storage
        self.role_storage = role_storage

    def get_user(self, username: str) -> User:
        """Get user by name."""

        db_user = self.user_storage.get_user(username=username)

        user = User.from_orm(db_user)

        return user

    def get_or_create_social_user(self, social_user: OAuthUser) -> User:
        """
        Return existing user by his social id.

        If user is not in database - create user with OAuth credentials.
        """
        db_social_account = self.user_storage.get_social_account(social_user.id)

        if not db_social_account:
            db_user = self.user_storage.create_user(
                username=social_user.username,
                email=social_user.email,
                password_hash=password_hash(app_config.SECRET_KEY))
            db_social_account = self.user_storage.create_social_account(
                user=db_user, social_user=social_user)

        user: User = User.from_orm(db_social_account.user[0])

        return user

    def create_user(self, user: InputCreateUser) -> User:
        """Create new user."""

        db_user: User = self.user_storage.create_user(
            username=user.username,
            email=user.email,
            password_hash=password_hash(user.password),
        )

        created_user = User.from_orm(db_user)

        return created_user

    def update_user(self, user: InputUpdateUser) -> User:
        """Update available user."""

        user_data = dict()
        if user.username:
            user_data.setdefault("username", user.username)
        if user.email:
            user_data.setdefault("email", user.email)
        if user.password:
            user_data.setdefault("password_hash", password_hash(user.password))

        db_updated_user = self.user_storage.update_user(id=user.id, raw_data=user_data)

        updated_user = User.from_orm(db_updated_user)

        return updated_user

    def set_role(self, role: str, user_name: str):
        self.user_storage.set_user_role(role, user_name)

    def validate_password(self, user: User, password: str) -> None:
        """
        Password validation.

        Check if passed password hash is equal to password hash from database.
        """

        encoded_password = password_hash(password)
        if encoded_password != user.password_hash:
            raise exc.ApiLoginInvalidParamsException

        return None

    def create_access_history(self, user: User, user_agent: str) -> None:
        """Create user login history entry."""

        self.user_storage.store_user_login_history(user_id=user.id, info=user_agent)

        return None

    def get_user_access_history(self, user_id: UUID) -> list:
        """Get user login history by user id."""

        db_history = self.user_storage.get_user_history(user_id=user_id)

        history = []
        for item in db_history:
            history.append(
                AccessHistory(
                    user_agent=item.info,
                    datetime=item.created_at,
                )
            )

        return history

    def set_role_to_user(self, user_id: UUID, role_id: UUID) -> None:
        """Tie role to the user."""

        db_role = self.role_storage.get_role(role_id=role_id)
        db_user = self.user_storage.get_user(id=user_id)

        self.user_storage.set_role_to_user(user=db_user, role=db_role)

        return None

    def check_user_role(self, user_id: UUID, role_id: UUID) -> bool:
        """Check if user has a role."""
        db_user = self.user_storage.get_user(id=user_id)

        has_role = False
        for role in db_user.roles:
            if str(role.id) == str(role_id):
                has_role = True
                break

        return has_role

    def delete_user_role(self, user_id: UUID, role_id: UUID) -> None:
        """Untie role from user."""
        db_role = self.role_storage.get_role(role_id=role_id)
        db_user = self.user_storage.get_user(id=user_id)

        self.user_storage.delete_user_role(user=db_user, role=db_role)

        return None
