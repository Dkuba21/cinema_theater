import abc
import datetime
import logging
from typing import List
from uuid import UUID

from db import db
from exceptions import ApiTokenNotFoundException
from models.db.auth_model import RefreshJwt


class IJwtStorage:
    """Base abstract class of jwt tokens storage."""

    @abc.abstractmethod
    def store_refresh_token(
        self, jti: UUID, user_id: UUID, expire_time: datetime.datetime
    ) -> None:
        """Store refresh token in database."""

    @abc.abstractmethod
    def remove_refresh_token(self, jti: UUID) -> None:
        """Remove refresh token from database."""

    @abc.abstractmethod
    def get_refresh_token(self, jti: str) -> str:
        """Get refresh token by its jti from database."""

    @abc.abstractmethod
    def get_refresh_tokens_jti(self, user_id: str) -> List[str]:
        """Get all jti's of all given refresh token by user id."""

    @abc.abstractmethod
    def remove_refresh_tokens(self, tokens_jti: List[str]):
        """Remove all given refresh token entries from database."""


class PostgresJwtStorage(IJwtStorage):

    def remove_refresh_tokens(self, tokens_jti: List[str]):
        for jti in tokens_jti:
            db.session.query(RefreshJwt).filter(RefreshJwt.id == jti).delete()

        db.session.commit()

    def get_refresh_tokens_jti(self, user_id: str) -> List[str]:
        tokens: List[RefreshJwt] = RefreshJwt.query.filter(
            RefreshJwt.user_id == user_id
        ).all()

        all_jti = [str(token.id) for token in tokens]

        return all_jti

    def remove_refresh_token(self, jti: UUID) -> None:
        deleted_rows = (
            db.session.query(RefreshJwt).filter(RefreshJwt.id == jti).delete()
        )
        db.session.commit()

        if deleted_rows == 0:
            logging.warning(f"Attempt to remove not existing refresh token was made. "
                            f"jti: {jti}")

        return None

    def get_refresh_token(self, jti: str) -> RefreshJwt:
        token = RefreshJwt.query.filter(RefreshJwt.id == jti).first()
        if not token:
            raise ApiTokenNotFoundException

        return token

    def store_refresh_token(
        self, jti: UUID, user_id: UUID, expire_time: datetime.datetime
    ) -> None:
        token = RefreshJwt(
            id=jti,
            user_id=user_id,
            expire=expire_time,
        )
        db.session.add(token)
        db.session.commit()

        return None
