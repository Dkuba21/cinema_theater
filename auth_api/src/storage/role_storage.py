import abc
import logging
from uuid import UUID

import sqlalchemy as sa

import exceptions as exc
from db import db
from models.db.auth_model import Role


class IRoleStorage:
    """Role storage base abstract class."""

    @abc.abstractmethod
    def get_roles(self) -> list[Role]:
        """Get all available roles."""

    @abc.abstractmethod
    def get_role(self, role_id: UUID) -> Role:
        """Get one role by its is."""

    @abc.abstractmethod
    def create_role(self, raw_data: dict) -> Role:
        """Create a new role."""

    @abc.abstractmethod
    def update_role(self, role_id: UUID, raw_data: dict) -> Role:
        """Update existing role by its id."""

    @abc.abstractmethod
    def delete_role(self, role_id: UUID) -> None:
        """Remove existing role by its id."""


class PostgresRoleStorage(IRoleStorage):

    def get_roles(self) -> list[Role]:
        roles: list[Role] = Role.query.all()

        return roles

    def get_role(self, role_id: UUID) -> Role:
        role: Role = Role.query.filter(Role.id == role_id).first()

        if not role:
            raise exc.ApiRoleNotFoundException

        return role

    def create_role(self, raw_data: dict) -> Role:
        role = Role(**raw_data)
        db.session.add(role)

        try:
            db.session.commit()
        except sa.exc.IntegrityError as exception:
            raise exc.ApiRoleAlreadyExistsException(
                detail=exception.orig.diag.message_detail
            )

        db.session.refresh(role)

        return role

    def update_role(self, role_id: UUID, raw_data: dict) -> Role:
        stmt = sa.update(Role).filter(Role.id == role_id).values(**raw_data)

        try:
            db.session.execute(stmt)
            db.session.commit()
        except sa.exc.IntegrityError as exception:
            raise exc.ApiRoleNotFoundException(
                detail=exception.orig.diag.message_detail
            )

        role = self.get_role(role_id=role_id)

        return role

    def delete_role(self, role_id: UUID) -> None:
        deleted_rows = db.session.query(Role).filter(Role.id == role_id).delete()
        db.session.commit()

        if deleted_rows == 0:
            logging.warning(f"Attempt to delete not existing role was made."
                            f"role id: '{role_id}'")
            raise exc.ApiRoleNotFoundException

        return None
