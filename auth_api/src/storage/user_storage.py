import abc
from typing import Optional
from uuid import UUID

import sqlalchemy as sa

import exceptions as exc
from db import db
from exceptions import (
    ApiEmailInUseException,
    ApiLoginInUseException,
    ApiUserAlreadyExistsException,
    ApiUserNotFoundException,
)
from models.api.user import OAuthUser
from models.db.auth_model import LoginHistory, User, Role, SocialAccount


class IUserStorage:
    """User storage base abstract class."""

    @abc.abstractmethod
    def get_user(
        self, id: Optional[str] = None, username: Optional[str] = None
    ) -> User:
        """Get user by name or by id."""

    @abc.abstractmethod
    def get_user_history(self, user_id: UUID) -> list[LoginHistory]:
        """Get user access history by user id."""

    @abc.abstractmethod
    def get_social_account(self, social_user_id: str) -> SocialAccount:
        """Get social account data by id from OAuth provider."""

    @abc.abstractmethod
    def create_user(self, username: str, email: str, password_hash: str) -> User:
        """Create a new user."""

    @abc.abstractmethod
    def create_social_account(self, user: User,
                              social_user: OAuthUser) -> SocialAccount:
        """
        Create social accaount in database.

        :param user: user model
        :param social_user: user data from OAuth provider
        """

    @abc.abstractmethod
    def update_user(self, id: UUID, raw_data: dict) -> User:
        """Update existing user by user id."""

    @abc.abstractmethod
    def store_user_login_history(self, user_id: UUID, info: str) -> None:
        """Store user login history in database."""

    @abc.abstractmethod
    def set_user_role(self, role_name: str, user_name: str):
        """
        Set role to user by user name and user role.

        :param role_name: role name
        :param user_name: user name
        """

    @abc.abstractmethod
    def set_role_to_user(self, user: User, role: Role) -> None:
        """Set role to user by user and role model."""

    @abc.abstractmethod
    def delete_user_role(self, user: User, role: Role) -> None:
        """Remove user role."""


class PostgresUserStorage(IUserStorage):

    def create_user(self, username: str, email: str, password_hash: str) -> User:
        user = User(username=username, email=email, password_hash=password_hash)
        db.session.add(user)

        try:
            db.session.commit()
        except sa.exc.IntegrityError as exception:
            raise ApiUserAlreadyExistsException(
                detail=exception.orig.diag.message_detail
            )

        db.session.refresh(user)

        return user

    def update_user(self, id: UUID, raw_data: dict) -> User:
        stmt = sa.update(User).filter(User.id == id).values(**raw_data)

        try:
            db.session.execute(stmt)
            db.session.commit()
        except sa.exc.IntegrityError as exception:
            raise ApiUserNotFoundException(detail=exception.orig.diag.message_detail)

        user = self.get_user(id=id)

        return user

    def get_social_account(self, social_user_id: str) -> SocialAccount:
        """Получить социальный аккаунт пользователя из БД."""
        social_account = SocialAccount.query.filter(
            SocialAccount.social_id == social_user_id).first()

        return social_account

    def create_social_account(self, user: User,
                              social_user: OAuthUser) -> SocialAccount:
        social_account = SocialAccount(user=[user], social_id=social_user.id,
                                       social_name=social_user.provider_name)
        db.session.add(social_account)

        try:
            db.session.commit()
        except sa.exc.IntegrityError as exception:
            raise ApiUserAlreadyExistsException(
                detail=exception.orig.diag.message_detail
            )

        db.session.refresh(social_account)

        return social_account

    def _user_credentials_validation(self, username: str, email: str) -> None:
        existed_user = self.get_user(username=username)

        if existed_user:
            raise ApiLoginInUseException

        user_from_db = User.query.filter(User.email == email).first()

        if user_from_db:
            raise ApiEmailInUseException

    def get_user(
        self,
        id: Optional[UUID] = None,
        username: Optional[str] = None,
    ) -> User:
        if id:
            user_from_db = User.query.filter(User.id == id).first()
        elif username:
            user_from_db = User.query.filter(User.username == username).first()

        if not user_from_db:
            raise ApiUserNotFoundException

        return user_from_db

    def get_user_history(self, user_id: UUID) -> list[LoginHistory]:
        """Получить данные из login_history по ключу user_id."""

        return LoginHistory.query.filter(LoginHistory.user_id == user_id).all()

    def store_user_login_history(self, user_id: UUID, info: str) -> None:
        history = LoginHistory(user_id=user_id, info=info)
        db.session.add(history)
        db.session.commit()

        return None

    def set_user_role(self, role_name: str, user_name: str):
        role = db.session.query(Role).filter(Role.name == role_name).first()

        if not role:
            raise exc.ApiRoleNotFoundException

        user = db.session.query(User).filter(User.username == user_name).first()

        if not user:
            raise exc.ApiUserNotFoundException

        user.roles.append(role)
        db.session.add(user)
        db.session.commit()

    def set_role_to_user(self, user: User, role: Role) -> None:
        user.roles.append(role)
        db.session.commit()

        return None

    def delete_user_role(self, user: User, role: Role) -> None:
        user.roles.remove(role)
        db.session.commit()

        return None
