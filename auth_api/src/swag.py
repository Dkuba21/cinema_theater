from flasgger import Swagger
from flask import Flask

swag = Swagger(template_file="../docs/openapi.yaml")


def init_swagger(app: Flask) -> Swagger:
    swag.init_app(app)

    return swag
