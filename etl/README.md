# ETL service

ETL service just loads data from Postgres to Elasticsearch periodically.

### Settings

First, edit ```settings.py``` .

#### Attantion!!!
 - It is very important to set environment variables in ``.env`` properly
 - Be sure that admin panel project is already started. ETL service assumes postgresql database has been run with admin-panel service.

### Run

Run service with

```docker-compose up -d --build```

elasticsearch and etl service itself will be started
