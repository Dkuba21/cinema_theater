"""Модуль работы с логером."""
import time
import os

import logging

from settings import DEBUG

from datetime import datetime

import logging.handlers

if DEBUG:
    logging_level = logging.DEBUG
else:
    logging_level = logging.INFO


def logger_init():
    """Инициализация логера."""
    path = "log/"
    filename = "logfile.log"

    if not os.path.exists(os.path.join(os.getcwd(), path)):
        os.mkdir(os.path.join(os.getcwd(), path))

    # get logger
    logger = logging.getLogger()
    logger.setLevel(logging_level)

    # File handler
    logfilename = datetime.now().strftime("%Y%m%d_%H%M%S") + f"_{filename}"
    file = logging.handlers.TimedRotatingFileHandler(f"{path}{logfilename}", when="midnight", interval=1)
    fileformat = logging.Formatter("%(asctime)s [%(levelname)s]: %(name)s: %(message)s")
    file.setLevel(logging_level)
    file.setFormatter(fileformat)

    # Stream handler
    stream = logging.StreamHandler()
    streamformat = logging.Formatter("%(asctime)s [%(levelname)s]: %(name)s: %(message)s")
    stream.setLevel(logging_level)
    stream.setFormatter(streamformat)

    # Adding all handlers to the logs
    logger.addHandler(file)
    logger.addHandler(stream)
