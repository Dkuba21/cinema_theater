"""Загрузочный модуль. Запускает ETL процесс."""
import logging
import time

from logger import logger_init
from services.consumer import DataConsumer
from services.state import json_file_state_manager
from services.producer import DataProducer
from settings import MAIN_PERIOD

logger_init()
logger = logging.getLogger()


def main():
    """Процесс ETL."""
    data_getter = DataProducer(state_manager=json_file_state_manager)
    data_putter = DataConsumer()

    while True:

        total_data_length = 0

        # пайплайн обновления фильмов
        for update_process in [data_getter.get_person_movies_updated_data(),
                               data_getter.get_genre_movies_updated_data(),
                               data_getter.get_updated_movies_data()]:
            logger.info(f'current process: {update_process}')
            for movie_data in update_process:
                data_putter.write_data(movie_data)
                total_data_length += len(movie_data)

        # пайплайн обновления данных о персонах и жанрах
        for models in [data_getter.updated_persons, data_getter.updated_genres]:
            if models:
                data_putter.write_data(models)

        data_getter.reset()
        logger.info(f'Обновлено {total_data_length} записей фильмов')
        time.sleep(MAIN_PERIOD)


if __name__ == '__main__':
    logger.info('App starts')
    main()
