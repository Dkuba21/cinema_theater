"""Модели данных."""
from typing import List, Optional
from uuid import UUID, uuid4

from pydantic import BaseModel, Field, validator

MAX_RATING = 10


class Person(BaseModel):
    """Персоны."""

    id: UUID = Field(default_factory=uuid4)
    full_name: str

    @property
    def index_name(self):
        return 'person'


class Genre(BaseModel):
    """Персоны."""

    index_name = 'genre'

    id: UUID = Field(default_factory=uuid4)
    name: str
    description: Optional[str]

    @property
    def index_name(self):
        return 'genre'


class Movie(BaseModel):
    """Фильмы."""

    index_name = 'movies'

    id: UUID = Field(default_factory=uuid4)
    imdb_rating: Optional[float]
    genre: List[Genre] = []
    title: str
    description: Optional[str]
    directors: List[Person] = []
    actors: List[Person] = []
    writers: List[Person] = []
    actors_names: List[str] = []
    writers_names: List[str] = []

    @validator('description')
    def description_validator(cls, description: str):
        """Валидация описания фильма."""
        if not description:
            return
        return description

    @validator('genre')
    def genre_validator(cls, genre: str):
        """Валидация жанра."""
        if not genre:
            return
        return genre

    @validator('imdb_rating')
    def rating_validator(cls, rating: float):
        """Валидация рейтинга фильма."""
        if not rating:
            return
        if rating < 0:
            raise ValueError('Рейтинг не может быть отрицательным')
        if rating > 10:
            raise ValueError(f'Рейтинг не может быть больше {MAX_RATING}')
        return rating

    @property
    def index_name(self):
        return 'movies'
