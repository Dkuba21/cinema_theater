"""Потребитель данных."""
import json
import logging
import time
from typing import List, Union

from elasticsearch import helpers

from postgres_to_es.es_settings import MOVIES_INDEX_NAME, INDEX_BODY_MAP, GENRE_INDEX_NAME, PERSON_INDEX_NAME
from postgres_to_es.models import Movie, Person, Genre
from postgres_to_es.utils.es_utils import get_es_connection

logger = logging.getLogger()


class DataConsumer:
    """Класс записывает данные ETL."""

    def __init__(self):
        """Инициализация потребителя данных. Настрйока подключения и создание индекса при необходимости."""
        logger.debug('Инициализация ElasticSearch')

        self.__create_indexes()

    def __create_indexes(self):
        for index_name in [MOVIES_INDEX_NAME, GENRE_INDEX_NAME, PERSON_INDEX_NAME]:
            if not self.__connection.indices.exists(index=index_name):
                logger.info(f'Индекс "{index_name}" не найден!')
                self.__create_index(index_name=index_name,
                                    body=INDEX_BODY_MAP[index_name])
                logger.info(f'Индекс "{index_name}" создан!')

    def __create_index(self, index_name, body):
        logger.info(f'Создаем индекс {index_name}...')
        self.__connection.indices.create(index=index_name, ignore=400,
                                         body=body)

        while not self.__connection.indices.exists(index=index_name):
            logger.debug('Ожидание готовности индекса...')
            time.sleep(3)

        logger.debug('Индекс создан')

    def __bulk_json_data(self, models: Union[List[Movie], List[Person], List[Genre]],
                         _index: str, doc_type='_doc'):
        json_list = [model.json() for model in models]
        for doc in json_list:
            yield {
                "_index": _index,
                "_type": doc_type,
                "_id": json.loads(doc)["id"],
                "_source": doc
            }

    def write_data(self, models: Union[List[Movie], List[Person], List[Genre]]):
        """Записать данные фильмов."""
        self.__connection.indices.put_settings(index=models[0].index_name,
                                               body={"index.blocks.read_only_allow_delete": None})
        helpers.bulk(
            self.__connection,
            self.__bulk_json_data(models, _index=models[0].index_name)
        )

    @property
    def __connection(self):
        """Вернуть валидное подклчюение."""
        return get_es_connection()
