"""Модуль содержит сущности относящиеся к извлечению данных."""
import logging
from datetime import datetime
from typing import List, Optional

import psycopg2
from psycopg2.extras import _connection as connection

from postgres_to_es.models import Movie, Person, Genre
from postgres_to_es.settings import BUF_SIZE, DATETIME_STR_PATTERN, DEFAULT_ACQUISITION_DATETIME
from postgres_to_es.services.state import json_file_state_manager
from postgres_to_es.utils.constants import PERSON_LAST_UPDATE_DATETIME, GENRE_LAST_UPDATE_DATETIME, \
    GENRE_MOVIES_QUERY_OFFSET, ENTITY_LAST_UPDATE_MAP, PERSON_DB_TABLE_NAME, \
    MOVIE_DB_TABLE_NAME, DB_ACTOR_ROLE_NAME, DB_WRITER_ROLE_NAME, DB_DIRECTOR_ROLE_NAME
from postgres_to_es.utils.db_utils import get_db_connection

logger = logging.getLogger()


class DataProducer:
    """Класс предоставляющий данные для ETL."""

    def __init__(self, state_manager):
        """Инициализация извлекателя данных. Настройка подключения."""
        self.connection: connection = get_db_connection()
        self._state_manager = state_manager

        # сллварь вида {'movie_id': Movie model} для обновленных фильмов
        self._updated_movies: dict = {}

        # id фильмов которые уже были обработаны
        self.__processed_movie_ids = []

        # модели жанров, которые були обновлены
        self.updated_genres: List[Genre] = []

        # модели персон, которые були обновлены
        self.updated_persons: List[Person] = []

    def reset(self):
        """Сброс состояния."""
        self.__processed_movie_ids = []
        self.updated_genres = []
        self.updated_persons = []

        self.connection.close()

    def get_updated_movies_data(self) -> List[Movie]:
        """Вернуть список содержащий данные по каждому фильму, данные которого поменялись."""
        for movie_ids in self.__get_updated_movies_ids():

            if not movie_ids:
                return

            movie_ids = list(set(movie_ids).difference(set(self.__processed_movie_ids)))
            if not movie_ids:
                continue

            self.__processed_movie_ids.extend(movie_ids)

            movies_data = self.__get_movies_data(movie_ids)

            yield self.__transform_data(movies_data)

    def get_person_movies_updated_data(self) -> List[Movie]:
        """Вернуть спиосок содержащий данные по каждому фильму и жанрам, если данные персон обновлялись."""
        for person_ids in self.__get_person_ids():

            if not person_ids:
                return

            self.updated_persons.extend(self.__get_persons_models(person_ids))

            movie_ids = self.__get_person_movies_ids(person_ids)

            movie_ids = list(set(movie_ids).difference(set(self.__processed_movie_ids)))

            if not movie_ids:
                continue

            self.__processed_movie_ids.extend(movie_ids)

            movies_data = self.__get_movies_data(movie_ids)

            yield self.__transform_data(movies_data)

    def __get_persons_models(self, person_ids: List[str]) -> List[Person]:
        """Получить список моделей персон по списку id."""
        placeholder = "'" + "', '".join(person_ids) + "'"
        cmd = f"""SELECT id, full_name FROM content.person WHERE id IN ({placeholder});"""
        result = self.__process_query(cmd)

        persons = []
        for row in result:
            persons.append(Person(id=row['id'], full_name=row['full_name']))

        return persons

    def __get_genre_models(self, genre_ids: List[str]) -> List[Genre]:
        """Получить список моделей жанров по списку id."""
        placeholder = "'" + "', '".join(genre_ids) + "'"
        cmd = f"""SELECT id, name, description FROM content.genre WHERE id IN ({placeholder});"""
        result = self.__process_query(cmd)

        genres = []
        for row in result:
            genres.append(Genre(id=row['id'], name=row['name'],
                                description=row['description'] if row['description'] else ''))

        return genres

    def get_genre_movies_updated_data(self) -> List[Movie]:
        """Вернуть список с данными о фильмах и жанрах, если жанры обновлялись."""
        genre_ids = self.__get_genres_ids(self._state_manager.get_state(GENRE_LAST_UPDATE_DATETIME))

        if not genre_ids:
            return

        self.updated_genres.extend(self.__get_genre_models(genre_ids))

        for movie_ids in self.__get_genre_movies_ids(genre_ids):
            movie_ids = list(set(movie_ids).difference(set(self.__processed_movie_ids)))

            if not movie_ids:
                continue

            self.__processed_movie_ids.extend(movie_ids)

            movies_data = self.__get_movies_data(movie_ids)
            yield self.__transform_data(movies_data)

    def __get_genres_ids(self, _datetime: str):
        """
        Вернуть список id жанров которые были обновлены после указанной даты.

        :param _datetime: дата и время последнего получения списка персон
        :return: список id
        """
        if not _datetime:
            _datetime = DEFAULT_ACQUISITION_DATETIME

        datetime_treshhold = datetime.strptime(_datetime, DATETIME_STR_PATTERN)

        cmd = """SELECT id, updated_at FROM content.genre WHERE updated_at > %s ORDER BY updated_at;"""

        result = self.__process_query(cmd, (datetime_treshhold,))

        if not result:
            return

        last_date_in_batch = result[-1][1]

        json_file_state_manager.set_state(GENRE_LAST_UPDATE_DATETIME,
                                          last_date_in_batch.strftime(DATETIME_STR_PATTERN))

        return [genre_data[0] for genre_data in result]

    def __get_genre_movies_query(self, genre_ids: List[str], offset: int):
        placeholder = "'" + "', '".join(genre_ids) + "'"
        cmd = f"""SELECT fw.id, fw.updated_at FROM content.film_work fw LEFT JOIN content.genre_film_work gfw
                ON gfw.film_work_id = fw.id WHERE gfw.genre_id IN ({placeholder})
                ORDER BY fw.updated_at LIMIT {BUF_SIZE} OFFSET {offset};"""

        return cmd

    def __get_genre_movies_ids(self, genre_ids: list) -> List[str]:
        """
        Вернуть список id фильмов, в которых упоминаются выбранные жанры.

        :param genre_ids: спиоск id выбранных жанров
        :return: список id фильмов
        """
        offset = 0

        while True:

            result = self.__process_query(self.__get_genre_movies_query(genre_ids, offset))

            if not result:
                return

            yield [genre_movie_data[0] for genre_movie_data in result]

            offset += GENRE_MOVIES_QUERY_OFFSET

    def __get_person_ids(self):
        """Вернуть список id персон которые были обновлены после указанной даты.

        :param _datetime: дата и время последнего получения списка персон
        :return: список id
        """
        while True:

            datetime_threshold = datetime.strptime(self._state_manager.get_state(PERSON_LAST_UPDATE_DATETIME),
                                                   DATETIME_STR_PATTERN)

            result = self.__get_last_updated_data(PERSON_DB_TABLE_NAME, datetime_threshold)

            if not result:
                return

            yield result

    def __get_last_updated_data(self, entity: str, datetime_threshold: datetime):
        """
        Вернуть из базы данных данные последнего которые добавлялись с последнего обновления.

        :param cmd: SQL запрос
        :param datetime_threshold: пороговое значение даты и времени
        :param last_update_entity_key: маркер сущности которая обновляется
        :return: спиоск id сущностей которые были запрошены
        """
        cmd = f"""SELECT id, updated_at FROM content.{entity} WHERE updated_at > %s ORDER BY updated_at
                LIMIT {BUF_SIZE};"""
        result = self.__process_query(cmd, (datetime_threshold,))

        if not result:
            return

        last_date_in_batch = result[-1][1]
        json_file_state_manager.set_state(ENTITY_LAST_UPDATE_MAP[entity],
                                          last_date_in_batch.strftime(DATETIME_STR_PATTERN))

        return [person_data[0] for person_data in result]

    def __get_person_movies_ids(self, person_ids: list) -> List[str]:
        """
        Вернуть список id фильмов, в который принимали участие выбранные первосны.

        :param person_ids: спиоск id выбранных персон
        :return: список id фильмов
        """
        placeholder = "'" + "', '".join(person_ids) + "'"
        cmd = f"""SELECT fw.id, fw.updated_at FROM content.film_work fw LEFT JOIN content.person_film_work pfw
        ON pfw.film_work_id = fw.id WHERE pfw.person_id IN ({placeholder}) ORDER BY fw.updated_at;"""

        result = self.__process_query(cmd)

        return [person_movie_data[0] for person_movie_data in result]

    def __get_updated_movies_ids(self):
        """
        Вернуть список id фильмов которые были обновлены после указанной даты.

        :return: список id или None
        """
        while True:

            datetime_threshold = datetime.strptime(self._state_manager.get_state(PERSON_LAST_UPDATE_DATETIME),
                                                   DATETIME_STR_PATTERN)

            result = self.__get_last_updated_data(MOVIE_DB_TABLE_NAME, datetime_threshold)

            if not result:
                return

            yield result

    def __get_movies_data(self, movie_ids: List[str]) -> List[str]:
        """
        Вернуть всю информацию о фильмах, по их id.

        :param movie_ids: id выбранных фильмов
        :return: список данных о фильмах
        """
        placeholder = "'" + "', '".join(movie_ids) + "'"
        cmd = f"""SELECT fw.id as fw_id, fw.title, fw.description, fw.rating, fw.type, fw.created_at, fw.updated_at,
        pfw.role, p.id, p.full_name, g.name FROM content.film_work fw LEFT JOIN content.person_film_work pfw
        ON pfw.film_work_id = fw.id LEFT JOIN content.person p ON p.id = pfw.person_id LEFT JOIN
        content.genre_film_work gfw ON gfw.film_work_id = fw.id LEFT JOIN content.genre g ON g.id = gfw.genre_id
        WHERE fw.id IN ({placeholder});"""

        result = self.__process_query(cmd)

        return result

    def __process_query(self, cmd: str, *args):
        try:

            cur = self.connection.cursor()
            cur.execute("SELECT 1")
        except:
            self.connection = get_db_connection()
            cur = self.connection.cursor()

        cur.execute(cmd, args)
        return cur.fetchall()

    def __transform_data(self, movies_data: list) -> List[Movie]:
        """
        Преобразование массива фильмов в струкутрированый формат схемы данных.

        :return: список моделей фильмов
        """
        movies = []

        for movie_data in movies_data:

            movie = self.__movie_get_or_create(movie_data)

            self.__update_movies(movie_data['fw_id'], movie)

        for movie_id in self._updated_movies.keys():
            movie = self._updated_movies[movie_id]
            self.__set_movie_person_data(movie)
            self.__set_movie_genre_data(movie)
            movies.append(movie)

        self._updated_movies = {}

        return movies

    def __set_movie_person_data(self, movie: Movie):
        cmd = f"""SELECT p.id as p_id, p.full_name as p_name, p_fw.role as p_role FROM content.person p
        LEFT JOIN content.person_film_work p_fw ON p_fw.person_id = p.id WHERE p_fw.film_work_id = '{movie.id}';"""

        result = self.__process_query(cmd)

        for movie_person_data in result:
            if movie_person_data['p_role'] == DB_ACTOR_ROLE_NAME:
                movie.actors_names.append(movie_person_data['p_name'])
                movie.actors.append(Person(id=movie_person_data['p_id'],
                                           full_name=movie_person_data['p_name']))
            elif movie_person_data['p_role'] == DB_WRITER_ROLE_NAME:
                movie.writers_names.append(movie_person_data['p_name'])
                movie.writers.append(Person(id=movie_person_data['p_id'],
                                            full_name=movie_person_data['p_name']))
            elif movie_person_data['p_role'] == DB_DIRECTOR_ROLE_NAME:
                movie.directors.append(Person(id=movie_person_data['p_id'],
                                              full_name=movie_person_data['p_name']))

    def __set_movie_genre_data(self, movie: Movie):
        cmd = f"""SELECT g.id as g_id, g.name as g_name, g.description as g_desc FROM content.genre g
        LEFT JOIN content.genre_film_work g_fw ON g_fw.genre_id = g.id WHERE g_fw.film_work_id = '{movie.id}';"""

        result = self.__process_query(cmd)

        for movie_genre_data in result:
            movie.genre.append(Genre(id=movie_genre_data['g_id'], name=movie_genre_data['g_name'],
                                     description=movie_genre_data['g_desc']))

    def __update_movies(self, movie_id, movie: Movie) -> None:
        self._updated_movies[movie_id] = movie

    def __movie_get_or_create(self, movie_data: dict) -> Movie:
        if not movie_data['fw_id'] in self._updated_movies.keys():
            self._updated_movies[movie_data['fw_id']] = Movie(id=movie_data['fw_id'],
                                                              imdb_rating=movie_data['rating'],
                                                              title=movie_data['title'],
                                                              description=movie_data['description'])

        return self._updated_movies[movie_data['fw_id']]
