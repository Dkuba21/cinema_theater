"""Хранение состояния процесса."""
from typing import Any

from postgres_to_es.services.storage import BaseStorage, json_storage
from postgres_to_es.settings import DEFAULT_ACQUISITION_DATETIME


class State:
    """
    Класс для хранения состояния при работе с данными, чтобы постоянно не перечитывать данные с начала.

    Здесь представлена реализация с сохранением состояния в файл.
    В целом ничего не мешает поменять это поведение на работу с БД или распределённым хранилищем.
    """

    def __init__(self, storage: BaseStorage):
        """Инициализция класса."""
        self.storage = storage

    def set_state(self, key: str, value: Any) -> None:
        """Установить состояние для определённого ключа."""
        data = {key: value}
        if key:
            self.storage.save_state(data)

    def get_state(self, key: str) -> Any:
        """Получить состояние по определённому ключу."""
        state = self.storage.retrieve_state()

        if key in state.keys():
            return state[key]

        return DEFAULT_ACQUISITION_DATETIME


json_file_state_manager = State(storage=json_storage)
