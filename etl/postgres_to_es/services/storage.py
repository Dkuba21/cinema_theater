"""Хранилище данных."""
import abc
import json
import os.path

from typing import Optional

from postgres_to_es.settings import JSON_FILE_STORAGE_PATH
from postgres_to_es.utils.path_utils import mkdir_recursive


class BaseStorage:
    """Базовый абстрактный класс хранилища данных."""

    @abc.abstractmethod
    def save_state(self, state: dict) -> None:
        """Сохранить состояние в постоянное хранилище."""
        pass

    @abc.abstractmethod
    def retrieve_state(self) -> dict:
        """Загрузить состояние локально из постоянного хранилища."""
        pass


class JsonFileStorage(BaseStorage):
    """Класс хранилища данных в json файле."""

    def __init__(self, file_path: Optional[str] = None):
        """Инициализация класса."""
        self.file_path = file_path

        path_folder = os.path.split(self.file_path)[0]
        if not os.path.exists(path_folder):
            mkdir_recursive(path_folder)

    def save_state(self, state: dict) -> None:
        """Сохранить состояние в постоянное хранилище."""
        self.__check_or_create_file()

        with open(self.file_path, "r") as file:
            data = file.read()

        with open(self.file_path, "w") as file:
            if data:
                old_state = json.loads(data)
                new_state = {**old_state, **state}
                file.write(json.dumps(new_state))
            else:
                file.write(json.dumps(state))

    def __check_or_create_file(self):
        if not os.path.exists(self.file_path):
            with open(self.file_path, "w") as file:
                file.close()

    def retrieve_state(self) -> dict:
        """Загрузить состояние локально из постоянного хранилища."""
        self.__check_or_create_file()

        with open(self.file_path, "r") as file:
            data = file.read()
            if data:
                state = json.loads(data)
            else:
                state = {}
        return state


json_storage = JsonFileStorage(JSON_FILE_STORAGE_PATH)
