"""Настройки приложения."""
import os

from dotenv import load_dotenv

load_dotenv()

# период запуска ETL процесса (сек.)
MAIN_PERIOD = 5

# параметры подключения Postgres
DATABASE_PARAMS = {'dbname': os.environ.get('POSTGRES_DB'),
                   'user': os.environ.get('POSTGRES_USER'),
                   'password': os.environ.get('POSTGRES_PASSWORD'),
                   'host': os.environ.get('DB_HOST'),
                   'port': os.environ.get('DB_PORT')}

DB_CONNECTION_BASE_DELAY = 0.1
DB_CONNECTION_DELAY_MULTIPLICATOR = 2
DB_CONNECTION_MAX_DELAY = 10

# параметры подключения ElasticSearch
ES_HOST = os.environ.get('ES_HOST')
ES_PORT = os.environ.get('ES_PORT')

# размер буфера данных для ETL
BUF_SIZE = 2000

# шаблон строки даты и времени для сохранения значения из БД
DATETIME_STR_PATTERN = '%m/%d/%Y, %H:%M:%S.%f'

# путь к файлу, хранящему состояние
JSON_FILE_STORAGE_PATH = 'state/state.json'

# время с которого нужно производить выборку по-умолчанию, если в хранилище нет записи о последней выборке
DEFAULT_ACQUISITION_DATETIME = '07/16/2020, 20:14:09.514450'

# режим отладки
DEBUG = False
