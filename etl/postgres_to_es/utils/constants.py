"""Контстанты."""

PERSON_LAST_UPDATE_DATETIME = "persons_last_update"
GENRE_LAST_UPDATE_DATETIME = "genres_last_update"
MOVIE_LAST_UPDATE_DATETIME = "movies_last_update"

PERSON_DB_TABLE_NAME = 'person'
GENRE_DB_TABLE_NAME = 'genre'
MOVIE_DB_TABLE_NAME = 'film_work'

# наименования ролей персон в фильмах
DB_ACTOR_ROLE_NAME = 'actor'
DB_WRITER_ROLE_NAME = 'writer'
DB_DIRECTOR_ROLE_NAME = 'director'

# словарь вида {'таблица БД': "ключ последнего обвноелния"}
ENTITY_LAST_UPDATE_MAP = {PERSON_DB_TABLE_NAME: PERSON_LAST_UPDATE_DATETIME,
                          GENRE_DB_TABLE_NAME: GENRE_LAST_UPDATE_DATETIME,
                          MOVIE_DB_TABLE_NAME: MOVIE_LAST_UPDATE_DATETIME}

# смещение при выборке фильмов для изменившихся жанров
GENRE_MOVIES_QUERY_OFFSET = 100
