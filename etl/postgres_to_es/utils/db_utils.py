"""Утилиты для работы с базой данных."""

import logging

import backoff as backoff
import psycopg2
from psycopg2.extras import DictCursor

from postgres_to_es.settings import DATABASE_PARAMS

logger = logging.getLogger()


@backoff.on_exception(backoff.expo,
                      psycopg2.OperationalError)
def get_db_connection():
    """
    Вернуть валидное подключение к базе данных.

    Циклические попытки произвести подключение в случае ошибки.
    """
    connection = psycopg2.connect(**DATABASE_PARAMS, cursor_factory=DictCursor)
    logger.info('Postgres connected')
    return connection
