"""Утилиты для работы с ElasticSearch."""

import logging

import backoff
from elasticsearch import Elasticsearch

from postgres_to_es.settings import ES_HOST, ES_PORT

logger = logging.getLogger()


@backoff.on_exception(backoff.expo,
                      Exception)
def get_es_connection():
    """Подключится к ElasticSearch. В случае наличия существующего подключения - проверить его.

    Если подклчючение валидно - вернуть существующее покдлчюение.
    """

    _es = Elasticsearch([{'host': ES_HOST, 'port': ES_PORT}])
    if not _es.ping():
        raise Exception("Нет ответа от ElasticSearch")
    return _es
