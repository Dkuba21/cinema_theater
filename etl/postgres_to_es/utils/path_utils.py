"""Утилиты для работы с фаловыми путями."""
import os


def mkdir_recursive(path: str) -> None:
    """Рекурсивно создать папки, входящие в путь."""
    sub_path = os.path.dirname(os.path.join(os.getcwd(), path))
    print(sub_path)
    if not os.path.exists(sub_path):
        mkdir_recursive(sub_path)
    if not os.path.exists(path):
        os.mkdir(path)
