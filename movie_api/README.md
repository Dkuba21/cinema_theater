## Ссылки на сервисы
- https://github.com/excander/Async_API_sprint_1 
- https://github.com/excander/Admin_panel_sprint_2-1
- https://github.com/excander/ETL-1


## Сервис Async API

### Настройка

- Создайте файл ```.env``` на основе ```.env_example```
- Не забудь сменить названиями контейнеров:
DB_HOST=postgres
DB_PORT=5432

ES_HOST=elasticsearch
ES_PORT=9200

REDIS_HOST=redis
REDIS_PORT=6379

Переменные окружения ```ADMIN_PANEL_PROJECT_PATH``` и ```ETL_PROJECT_PATH``` должны содержать абсолютные пути к локальным репозиториям проектов админ-панели и ETL соответственно (необходимо предварительно клонировать репозитории локально)

### Запуск
- Запустите сервис admin_panel и postgres  
```docker-compose up -d --build admin_panel, postgres```  
- Примените миграции и создайте супер-пользователя  
```docker-compose exec admin_panel python manage.py migrate --noinput```   
```docker-compose exec admin_panel python manage.py createsuperuser```  
```docker-compose exec admin_panel python manage.py collectstatic```
  
- Запустите остальные сервисы  
```docker-compose up -d --build```


После запуска на локальном хосте появятся:  
- 8000 порт - сервис панели администратора
- 8001 порт - асинхронный сервис
- 5432 - БД postgres
- 9200 - Elasticsearch
- 5601 - Kibana

внесите изменения в ```docker-compose.yml``` для другой конфигурации