from http import HTTPStatus
from typing import List, Optional
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, Query, Header
from fastapi_cache.decorator import cache

from core.config import CACHE_EXPIRE
from models import film as film_model
from models.film import FilmOutput, FilmFieldSort
from services.film import FilmService, get_film_service
from models.common import SimpleSort, OrderSort


def make_sort_obj(sort_by: str) -> SimpleSort:
    # парсим параметры для сортировки
    if sort_by.startswith("-"):
        sort_field = sort_by.removeprefix("-")
        sort_order = OrderSort.desc.value
    else:
        sort_field = sort_by
        sort_order = OrderSort.asc.value

    # создаем объект сортировки
    return SimpleSort(field=sort_field, order=sort_order)


router = APIRouter()


@router.get(
    "/search",
    response_model=List[FilmOutput],
    summary="List of films of query ranked accordingly search pattern",
    response_description="Relevant film list",
)
@cache(expire=CACHE_EXPIRE)
async def search_films(
    page_size: int = Query(50, alias="page[size]"),
    page_number: int = Query(1, alias="page[number]"),
    search_template: str = Query(..., alias="query"),
    film_service: FilmService = Depends(get_film_service),
) -> List[FilmOutput]:

    if page_size < 0 or page_number < 0:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail="ensure this value is greater than or equal to 1",
        )

    _films = await film_service.get_many(
        page_size=page_size,
        page_number=page_number,
        search_template=search_template,
    )

    if not _films:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="films not found")

    return [
        FilmOutput(id=film.id, title=film.title, imdb_rating=film.imdb_rating)
        for film in _films
    ]


@router.get(
    "/",
    response_model=List[FilmOutput],
    summary="Film info list ranked accordingly",
    description="Film info by id",
    response_description="Relevant film list",
)
@cache(expire=CACHE_EXPIRE)
async def films(
    sort: FilmFieldSort = Query(
        FilmFieldSort.imdb_rating_desc, description="Поле для сортировки", alias="sort"
    ),
    filter_genre: UUID = Query("", alias="filter[genre]"),
    page_size: int = Query(50, alias="page[size]"),
    page_number: int = Query(1, alias="page[number]"),
    film_service: FilmService = Depends(get_film_service),
) -> List[FilmOutput]:

    sort_obj = make_sort_obj(sort)

    _films = await film_service.get_many(
        page_size=page_size,
        page_number=page_number,
        sort=sort_obj,
        filter_genre=str(filter_genre),
    )

    if not _films:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="films not found")

    return [
        FilmOutput(id=film.id, title=film.title, imdb_rating=film.imdb_rating)
        for film in _films
    ]


@router.get(
    "/{film_id}",
    response_model=film_model.FilmOutputFull,
    summary="One film info",
    description="Film info by id",
    response_description="Relevant film info",
)
@cache(expire=CACHE_EXPIRE)
async def film_details(
    film_id: UUID, film_service: FilmService = Depends(get_film_service)
) -> film_model.FilmOutputFull:
    film = await film_service.get_by_id(film_id)
    if not film:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="film not found")

    return film_model.FilmOutputFull.parse_obj(
        film.dict(exclude={"MAX_RATING", "index_name"})
    )
