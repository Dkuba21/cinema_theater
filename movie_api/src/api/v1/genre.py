from http import HTTPStatus
from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from fastapi_cache.decorator import cache

from core.config import CACHE_EXPIRE
from models import genre as genre_model
from services.genre import GenreService, get_genre_service

router = APIRouter()


@router.get('/', response_model=List[genre_model.Genre],
            summary='Genre list',
            description='All existing genre list',
            response_description='Relevant genre list'
            )
@cache(expire=CACHE_EXPIRE)
async def all_genres(genre_service: GenreService = Depends(get_genre_service)) -> List[genre_model.Genre]:
    genres = await genre_service.get_all_genres()
    if not genres:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail='genres not found')

    return genres


@router.get('/{genre_id}', response_model=genre_model.Genre,
            summary='One genre info',
            description='Genre info by genre id',
            response_description='Relevant genre info'
            )
@cache(expire=CACHE_EXPIRE)
async def genre_details(genre_id: UUID, genre_service: GenreService = Depends(get_genre_service)) -> genre_model.Genre:
    genre = await genre_service.get_by_id(genre_id)
    if not genre:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail='genre not found')

    return genre
