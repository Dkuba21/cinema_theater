from http import HTTPStatus
from typing import List, Optional
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from fastapi_cache.coder import JsonCoder
from fastapi_cache.decorator import cache

from core.config import CACHE_EXPIRE
from models.film import FilmOutput
from models.person import PersonOutput
from services.person import PersonService, get_person_service

router = APIRouter()


def my_key_builder(
    func,
    *args,
    **kwargs,
):
    cache_key = f"{func.__name__}:{str(kwargs['kwargs'].get('person_id'))}"
    return cache_key


@router.get('/{person_id}', response_model=List[PersonOutput],
            summary='One person info',
            description='Person info by person id',
            response_description='Relevant person info'
            )
@cache(expire=CACHE_EXPIRE, coder=JsonCoder, key_builder=my_key_builder)
async def person_details(person_id: UUID,
                         person_service: PersonService = Depends(get_person_service)) -> Optional[List[PersonOutput]]:
    person_data = await person_service.get_by_id(person_id)
    if not person_data:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail='person not found')

    return person_data


@router.get('/{person_id}/film', response_model=List[FilmOutput],
            summary='Persons films list',
            description='Film list where person took part',
            response_description='Relevant films list'
            )
@cache(expire=CACHE_EXPIRE, coder=JsonCoder, key_builder=my_key_builder)
async def person_films(person_id: UUID,
                       person_service: PersonService = Depends(get_person_service)) -> Optional[List[FilmOutput]]:
    films = await person_service.get_all_person_films(person_id)
    if not films:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail='person not found')

    return films
