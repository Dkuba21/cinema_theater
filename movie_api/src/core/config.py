import os
from logging import config as logging_config

from src.core.logger import LOGGING

# from dotenv import load_dotenv

# dirpath = os.path.abspath(os.path.dirname(__file__))
# PATH = os.path.join(dirpath, ".env")
# print(PATH)
# load_dotenv(PATH)

# Применяем настройки логирования
logging_config.dictConfig(LOGGING)

# Название проекта. Используется в Swagger-документации
PROJECT_NAME = os.getenv('PROJECT_NAME', 'movies')

# Настройки Redis
REDIS_HOST = os.getenv('REDIS_HOST', '127.0.0.1')
REDIS_PORT = int(os.getenv('REDIS_PORT', 6379))
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', 'qwerty')
REDIS_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}"

# Настройки Elasticsearch
ELASTIC_HOST = os.getenv('ES_HOST', '127.0.0.1')
ELASTIC_PORT = int(os.getenv('ES_PORT', 9200))

# Корень проекта
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

CACHE_EXPIRE = int(os.getenv('CACHE_EXPIRE', 5*60))
