from enum import Enum
from pydantic import BaseModel, Field


class OrderSort(str, Enum):
    """виды направления сортировки"""

    asc = "asc"
    desc = "desc"


class SimpleSort(BaseModel):
    """базовая модель для сортировок"""

    field: str = Field(..., description="Поле для сортировки")
    order: OrderSort = Field(OrderSort.desc.value, description="Направление сортировки")