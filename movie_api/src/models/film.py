from typing import List, Optional
from uuid import UUID, uuid4

# Используем pydantic для упрощения работы при перегонке данных из json в объекты
from pydantic import Field, validator

from models.base import BaseServiceModel
from models.genre import Genre
from models.person import Person
from enum import Enum

class Film(BaseServiceModel):
    """Фильмы."""

    MAX_RATING = 10

    index_name = 'movies'

    id: UUID = Field(default_factory=uuid4)
    imdb_rating: Optional[float]
    genre: List[Genre] = []
    title: str
    description: Optional[str]
    directors: List[Person] = []
    actors: List[Person] = []
    writers: List[Person] = []
    actors_names: List[str] = []
    writers_names: List[str] = []

    @validator('description')
    def description_validator(cls, description: str):
        """Валидация описания фильма."""
        if not description:
            return None
        return description

    @validator('genre')
    def genre_validator(cls, genre: str):
        """Валидация жанра."""
        if not genre:
            return None
        return genre

    @validator('imdb_rating')
    def rating_validator(cls, rating: float):
        """Валидация рейтинга фильма."""
        if not rating:
            return None
        if rating < 0:
            raise ValueError('Рейтинг не может быть отрицательным')
        if rating > 10:
            raise ValueError(f'Рейтинг не может быть больше {cls.MAX_RATING}')
        return rating


class FilmOutputFull(BaseServiceModel):
    """Модель данных содержащая полную ифномрацию о фильме (без солужебных полей)."""

    id: UUID = Field(default_factory=uuid4)
    imdb_rating: Optional[float]
    genre: List[Genre] = []
    title: str
    description: Optional[str]
    directors: List[Person] = []
    actors: List[Person] = []
    writers: List[Person] = []
    actors_names: List[str] = []
    writers_names: List[str] = []

    @validator('description')
    def description_validator(cls, description: str):
        """Валидация описания фильма."""
        if not description:
            return None
        return description

    @validator('genre')
    def genre_validator(cls, genre: str):
        """Валидация жанра."""
        if not genre:
            return None
        return genre


class FilmOutput(BaseServiceModel):
    """Модель фильмов для вывода данных в API."""
    id: UUID
    title: str
    imdb_rating: float



class FilmFieldSort(str, Enum):
    """Список полей доступных для сортировки фильмов."""

    imdb_rating_desc = "-imdb_rating"
    imdb_rating_asc = "imdb_rating"
    title_desc = "-title.raw"
    title_asc = "title.raw"