from typing import Optional
from uuid import UUID, uuid4

from pydantic import Field

from models.base import BaseServiceModel


class Genre(BaseServiceModel):
    """Жанры."""

    id: UUID = Field(default_factory=uuid4)
    name: str
    description: Optional[str]
