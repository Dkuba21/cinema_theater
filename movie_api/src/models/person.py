from typing import List
from uuid import UUID, uuid4

from pydantic import BaseModel, Field


class Person(BaseModel):
    """Персоны."""

    id: UUID = Field(default_factory=uuid4)
    full_name: str


class PersonOutput(BaseModel):
    """Персоны. Модель для выдачи на выходе API"""

    id: UUID = Field(default_factory=uuid4)
    full_name: str
    role: str
    film_ids: List[UUID]
