from functools import lru_cache
from typing import List, Optional
from uuid import UUID

from aioredis import Redis
from elasticsearch import AsyncElasticsearch
from fastapi import Depends

from db.elastic import get_elastic
from db.redis import get_redis
from models.film import Film


class FilmService:
    def __init__(self, redis: Redis, elastic: AsyncElasticsearch):
        self.redis = redis
        self.elastic = elastic

    async def get_many(self, page_size: int, page_number: int, high_rating_first=True,
                       search_template: str = '', filter_genre: str = '') -> Optional[Film]:
        """
        Вернуть список фильмов.

        :param filter_genre: фильтр по жанру
        :param search_template: шаблон поиска
        :param page_size: размер страницы
        :param page_number: номер страницы (смещение)
        :param high_rating_first: флаг порядка сортировки (по-умолчанию сначала идут фильмы с высоким рейтингом)
        :return:
        """
        films = await self._get_films_from_elastic(page_size=page_size, page_number=page_number,
                                                   high_rating_first=high_rating_first,
                                                   filter_genre=filter_genre,
                                                   film_search_template=search_template)
        return films

    async def get_by_id(self, film_id: UUID) -> Optional[Film]:
        """Вернуть данные фильма по id."""
        film = await self._get_film_from_elastic(film_id)
        if not film:
            return None

        return film

    async def _get_film_from_elastic(self, film_id: UUID) -> Optional[Film]:
        doc = await self.elastic.get('movies', str(film_id))
        return Film(**doc['_source'])

    async def _get_films_from_elastic(self, page_size: int, page_number: int, filter_genre: str,
                                      film_search_template: Optional[str] = '',
                                      high_rating_first: bool = True) -> Optional[List[Film]]:

        sort_order = 'desc' if high_rating_first else 'asc'

        query_params = {"query": {"query_string": {}}}
        if film_search_template:
            query_params["query"]["query_string"] = {"query": film_search_template, "fields": ["title", "description"]}
        elif filter_genre:
            query_params["query"]["query_string"] = {"query": filter_genre, "fields": ["genre.id", ]}
        else:
            query_params = {"query": {"match_all": {}}}

        sort_params = {"sort": {"imdb_rating": sort_order}}

        request_body = {**query_params, **sort_params}

        _from = page_number * page_size

        doc = await self.elastic.search(index='movies', body=request_body, size=page_size,
                                        from_=_from)

        return [Film(**doc['_source']) for doc in doc["hits"]["hits"]]


@lru_cache()
def get_film_service(
        redis: Redis = Depends(get_redis),
        elastic: AsyncElasticsearch = Depends(get_elastic),
) -> FilmService:
    return FilmService(redis, elastic)
