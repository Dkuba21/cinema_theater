import json
from functools import lru_cache
from typing import List, Optional
from uuid import UUID

from aioredis import Redis
from elasticsearch import AsyncElasticsearch
from fastapi import Depends

from core.const import ACTOR_ROLE_NAME, DIRECTOR_ROLE_NAME, WRITER_ROLE_NAME
from db.elastic import get_elastic
from db.redis import get_redis
from models.film import Film, FilmOutput
from models.person import Person, PersonOutput

PERSON_ROLE_MAP = {ACTOR_ROLE_NAME: [], WRITER_ROLE_NAME: [], DIRECTOR_ROLE_NAME: []}


class PersonService:

    def __init__(self, redis: Redis, elastic: AsyncElasticsearch):
        self.redis = redis
        self.elastic = elastic

    async def get_all_person_films(self, person_id: UUID) -> Optional[List[FilmOutput]]:
        """Вернуть список данных всех фильмов, относящихся к персоне."""
        films = await self._get_all_person_movies_from_elasticsearch(person_id)

        return films

    async def _get_all_person_movies_from_elasticsearch(self, person_id: UUID) -> Optional[List[Film]]:
        """Вернуть все фильмы из elasticsearch в которых персона принимала участие в любой роли."""
        query_body = {"query": {"nested": {"path": "actors", "query": {"bool": {"should": [
                                {"match": {"actors.id": person_id}},
                                {"match": {"writers.id": person_id}},
                                {"match": {"directors.id": person_id}}]}}}}}

        doc = await self.elastic.search(index='movies', body=query_body)

        films = []
        for doc in doc['hits']['hits']:
            films.append(FilmOutput(id=doc['_source']['id'], title=doc['_source']['title'],
                                    imdb_rating=doc['_source']['imdb_rating']))

        return films

    async def get_by_id(self, person_id: UUID) -> Optional[List[PersonOutput]]:
        """Вернуть список данных о фильмах и ролях для одной персоны."""
        _person = await self._get_person_from_elastic(person_id)
        if not _person:
            return None
        person_data = []
        PERSON_ROLE_MAP[ACTOR_ROLE_NAME].extend(
            await self._get_all_person_actor_films_from_elasticsearch(person_id))
        PERSON_ROLE_MAP[WRITER_ROLE_NAME].extend(
            await self._get_all_person_writer_films_from_elasticsearch(person_id))
        PERSON_ROLE_MAP[DIRECTOR_ROLE_NAME].extend(
            await self._get_all_person_director_films_from_elasticsearch(person_id))
        for role_name in PERSON_ROLE_MAP.keys():
            if PERSON_ROLE_MAP[role_name]:
                person_data.append(PersonOutput(id=person_id, full_name=_person.full_name, role=role_name,
                                                film_ids=PERSON_ROLE_MAP[role_name]))
        return person_data

    async def _get_person_from_elastic(self, person_id: UUID) -> Optional[Person]:
        doc = await self.elastic.get('person', str(person_id))
        return Person(**doc['_source'])

    async def _get_all_person_actor_films_from_elasticsearch(self, person_id: UUID):
        """Вернуть все фильмы в которых персона принимала участие как актер"""

        query_body = {"query": {"nested": {"path": "actors", "query": {"bool": {"must":
                                {"match": {"actors.id": person_id}}}}}}}

        doc = await self.elastic.search(index='movies', body=query_body)

        films = []
        for film in doc['hits']['hits']:
            films.append(film['_source']['id'])

        return films

    async def _get_all_person_writer_films_from_elasticsearch(self, person_id: UUID):
        """Вернуть все фильмы в которых персона принимала участие как сценарист"""

        query_body = {"query": {"nested": {"path": "writers", "query": {"bool": {"must":
                                {"match": {"writers.id": person_id}}}}}}}

        doc = await self.elastic.search(index='movies', body=query_body)

        films = []
        for film in doc['hits']['hits']:
            films.append(film['_source']['id'])

        return films

    async def _get_all_person_director_films_from_elasticsearch(self, person_id: UUID):
        """Вернуть все фильмы в которых персона принимала участие как режиссер"""

        query_body = {"query": {"bool": {"must": {"match": {"directors.id": person_id}}}}}

        doc = await self.elastic.search(index='movies', body=query_body)

        films = []
        for film in doc['hits']['hits']:
            films.append(film['_source']['id'])

        return films


@lru_cache()
def get_person_service(
        redis: Redis = Depends(get_redis),
        elastic: AsyncElasticsearch = Depends(get_elastic),
) -> PersonService:
    return PersonService(redis, elastic)
