import asyncio
import json
import typing as t
from dataclasses import dataclass
from typing import List

import aiohttp
import aioredis
import pytest as pytest
from elasticsearch import AsyncElasticsearch, helpers
from multidict import CIMultiDictProxy

from tests.functional.settings import (ASYNC_API_HOST, ASYNC_API_PORT, ES_HOST,
                                       ES_PORT, REDIS_URL)
from tests.functional.utils.es_settings import (GENRE_INDEX_NAME,
                                                INDEX_BODY_MAP,
                                                MOVIES_INDEX_NAME,
                                                PERSON_INDEX_NAME)
from tests.functional.utils.test_data import get_test_data

SERVICE_URL = f"http://{ASYNC_API_HOST}:{ASYNC_API_PORT}"

DATA_IDS_MAP = {MOVIES_INDEX_NAME: [], GENRE_INDEX_NAME: [], PERSON_INDEX_NAME: []}


def __bulk_json_data(data: List[dict], _index: str, doc_type="_doc"):
    for doc in data:
        DATA_IDS_MAP[_index].append(doc["id"])
        yield {
            "_index": _index,
            "_type": doc_type,
            "_id": doc["id"],
            "_source": json.dumps(doc),
        }


@dataclass
class HTTPResponse:
    body: dict
    headers: CIMultiDictProxy[str]
    status: int


@pytest.fixture(scope="session")
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def es_client():
    """Клиент для работы с ES"""
    client = AsyncElasticsearch(hosts=f"{ES_HOST}:{ES_PORT}")
    yield client
    await client.close()


@pytest.fixture(scope="session", autouse=True)
async def load_data_es(es_client):
    for index_name in [MOVIES_INDEX_NAME, GENRE_INDEX_NAME, PERSON_INDEX_NAME]:

        if not await es_client.indices.exists(index=index_name):
            if not await es_client.indices.exists(index=index_name):
                await es_client.indices.create(
                    index=index_name, ignore=400, body=INDEX_BODY_MAP[index_name]
                )

        body = []

        for entry in get_test_data(index_name):
            body.append(entry)

        await helpers.async_bulk(
            es_client, __bulk_json_data(body, _index=index_name), refresh=True
        )
    yield
    for index_name in [MOVIES_INDEX_NAME, GENRE_INDEX_NAME, PERSON_INDEX_NAME]:
        for _id in DATA_IDS_MAP[index_name]:
            await es_client.delete(index=index_name, doc_type='_doc', id=_id)


@pytest.fixture(scope="session")
async def session():
    session = aiohttp.ClientSession()
    yield session
    await session.close()


@pytest.fixture
def make_get_request(session):
    """Конструкция для выполнения http запроса и получения ответа"""

    async def inner(method: str, params: t.Optional[dict] = None) -> HTTPResponse:
        params = params or {}
        method = method.lstrip("/")
        url = f"{SERVICE_URL}/api/v1/{method}"
        async with session.get(url, params=params) as response:
            return HTTPResponse(
                body=await response.json(),
                headers=response.headers,
                status=response.status,
            )

    return inner


@pytest.fixture(scope="session", autouse=True)
async def redis_client():
    """Создаем клиент редиса и сбрасываем кеш в начале сеанса тестирования"""
    redis = aioredis.from_url(REDIS_URL, encoding="utf-8", decode_responses=True)
    await redis.flushall()
    yield redis
    await redis.close()


@pytest.fixture(autouse=True)
async def redis_flush(redis_client):
    """очищаем кеш редиса перед каждым тестом"""
    await redis_client.flushall()
