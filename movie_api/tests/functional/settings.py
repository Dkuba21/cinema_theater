"""Настройки приложения."""
import os

from dotenv import load_dotenv

load_dotenv()

# параметры подключения ElasticSearch
ES_HOST = os.environ.get('ES_HOST')
ES_PORT = os.environ.get('ES_PORT')

REDIS_HOST = os.environ.get('REDIS_HOST')
REDIS_PORT = os.environ.get('REDIS_PORT')
REDIS_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}"

ASYNC_API_HOST = os.environ.get('ASYNC_API_HOST')
ASYNC_API_PORT = os.environ.get('ASYNC_API_PORT')

print(ES_HOST)
