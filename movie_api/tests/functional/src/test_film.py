from http import HTTPStatus
import pytest

from tests.functional.utils.es_settings import MOVIES_INDEX_NAME
from tests.functional.utils.test_data import get_test_data_sample_by_id

pytestmark = pytest.mark.asyncio


async def test_search_detailed(make_get_request):
    """Тест поиска фильмов по шаблону поиска."""
    response = await make_get_request("/film/search", {"query": "war"})
    expected = {
        "id": "983e0b41-dd17-4fd6-b4e7-771f975fdc19",
        "imdb_rating": 7.9,
        "title": "Star Wars: The Force Unleashed",
    }

    assert response.status == HTTPStatus.OK
    assert len(response.body) == 22
    assert response.body[0] == expected


async def test_get_single_film(make_get_request):
    """Тест получения фильма по id."""
    response = await make_get_request("/film/d38bced9-f39d-47b5-ad4c-5052d76c0246")

    expected = get_test_data_sample_by_id(
        MOVIES_INDEX_NAME, "d38bced9-f39d-47b5-ad4c-5052d76c0246"
    )

    assert response.status == HTTPStatus.OK
    assert len(response.body) == 10
    assert response.body == expected


async def test_film_list(make_get_request):
    """Тест получения списка фильмов."""
    response = await make_get_request("/film/")
    expected = {
        "id": "d38bced9-f39d-47b5-ad4c-5052d76c0246",
        "imdb_rating": 7.7,
        "title": "Star Trek: The Experience - The Klingon Encounter",
    }

    assert response.status == HTTPStatus.OK
    assert len(response.body) == 50
    assert response.body[0] == expected

test_sort_case = [
    ("imdb_rating", "imdb_rating", 6.9),
    ("-imdb_rating", "imdb_rating", 7.7),
]


@pytest.mark.parametrize(
    "sort_param, sort_field, expected_value",
    test_sort_case,
    ids=[f"field: {x[0]}" for x in test_sort_case],
)
async def test_film_sort(make_get_request, sort_param, sort_field, expected_value):
    """Тест сортировки по определенным полям запрашиваемых данных по фильмам"""
    params = {
        "sort": sort_param,
    }
    response = await make_get_request(method="/film", params=params)

    assert response.status == HTTPStatus.OK
    assert response.body[0][sort_field] == expected_value


async def test_film_sort_wrong_field(make_get_request):
    """Тест сортировки при передачи поля для которого не настроена сортировка или несуществующего поля"""
    params = {
        "sort": "some_field",
    }
    response = await make_get_request(method="/film", params=params)

    assert response.status == HTTPStatus.UNPROCESSABLE_ENTITY

async def test_film_list_validation_parametrs(make_get_request):
    """Тест валидации параметров пагинации"""
    params = {
        "page[size]": -10,
        "page[number]": -5,
        "query":'war',
    }
    response = await make_get_request(method="/film/search", params=params)

    assert response.status == HTTPStatus.BAD_REQUEST
    assert response.body["detail"] == "ensure this value is greater than or equal to 1"


async def test_film_404(make_get_request):
    """Тест запроса фильма с несуществующим uuid"""
    response = await make_get_request(
        method=f"/film/00000000-0000-0000-0000-000000000000"
    )

    assert response.status == HTTPStatus.NOT_FOUND
    assert response.body["detail"] == "film not found"
