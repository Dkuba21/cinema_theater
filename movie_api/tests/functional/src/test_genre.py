import pytest
from http import HTTPStatus

from tests.functional.utils.es_settings import GENRE_INDEX_NAME
from tests.functional.utils.test_data import get_test_data_sample_by_id

pytestmark = pytest.mark.asyncio


async def test_get_single_genre(make_get_request):
    """Тест получения жанра по id."""
    response = await make_get_request("/genre/120a21cf-9097-479e-904a-13dd7198c1dd")

    expected = get_test_data_sample_by_id(
        GENRE_INDEX_NAME, "120a21cf-9097-479e-904a-13dd7198c1dd"
    )

    assert response.status == HTTPStatus.OK
    assert len(response.body) == 3
    assert response.body == expected


async def test_genre_list(make_get_request):
    """Тест получения списка жанров."""
    response = await make_get_request("/genre/")
    expected = {
        "description": "",
        "id": "3d8d9bf5-0d90-4353-88ba-4ccc5d2c07ff",
        "name": "Action",
    }

    assert response.status == HTTPStatus.OK
    assert len(response.body) == 10
    assert response.body[0] == expected


async def test_genre_list_default_pagination(make_get_request):
    """Тест получения списка жанров без передачи параметров"""
    response = await make_get_request(method="/genre")

    assert response.status == HTTPStatus.OK

async def test_genre_404(make_get_request):
    """Тест запроса жанра с несуществующим uuid"""
    response = await make_get_request(
        method=f"/genre/00000000-0000-0000-0000-000000000000"
    )

    assert response.status == HTTPStatus.NOT_FOUND
    assert response.body["detail"] == "genre not found"
