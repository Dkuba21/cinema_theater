import pytest
from http import HTTPStatus
from tests.functional.utils.es_settings import PERSON_INDEX_NAME
from tests.functional.utils.test_data import get_test_data_sample_by_id

pytestmark = pytest.mark.asyncio


@pytest.mark.asyncio
async def test_search_person_detailed(make_get_request):
    """Тест поиска фильмов по id персоны."""
    person_id = "5b4bf1bc-3397-4e83-9b17-8b10c6544ed1"

    response = await make_get_request(f"/person/{person_id}/film")
    expected = {
        "id": "3d825f60-9fff-4dfe-b294-1a45fa1e115d",
        "imdb_rating": 8.6,
        "title": "Star Wars: Episode IV - A New Hope",
    }

    assert response.status == HTTPStatus.OK
    assert len(response.body) == 10
    assert response.body[0] == expected


@pytest.mark.asyncio
async def test_get_single_person(make_get_request):
    """Тест получения данных для персоны по id."""
    response = await make_get_request("/person/5b4bf1bc-3397-4e83-9b17-8b10c6544ed1")

    assert response.status == HTTPStatus.OK
    assert len(response.body) == 1
    assert response.body[0]["id"] == "5b4bf1bc-3397-4e83-9b17-8b10c6544ed1"
    assert response.body[0]["full_name"] == "Harrison Ford"
    assert len(response.body[0]["film_ids"]) == 10

async def test_person_404(make_get_request):
    """Тест запроса персоны с несуществующим uuid"""
    response = await make_get_request(
        method=f"/person/00000000-0000-0000-0000-000000000000"
    )

    assert response.status == HTTPStatus.NOT_FOUND
    assert response.body["detail"] == "person not found"
