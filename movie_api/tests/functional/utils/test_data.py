import json
import os
from typing import List


def get_test_data(index_name: str) -> List[dict]:
    """Получить тестовые данные из файла для заданного индекса."""
    test_data_filepath = os.path.join(os.getcwd(), 'tests', 'functional', 'testdata', f'{index_name}.json')

    with open(test_data_filepath) as file:
        data = json.load(file)['data']

    return data


def get_test_data_sample_by_id(index_name:str, id: str) -> dict:
    """Получить одну запись тестовых данных по id."""
    for entry in get_test_data(index_name):
        if entry['id'] == id:
            return entry
