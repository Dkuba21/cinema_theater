"""Ожидание готовности сервиса Elasticsearch."""
import asyncio

import backoff
from elasticsearch import AsyncElasticsearch

from tests.functional.settings import ES_HOST, ES_PORT


class EsNotAvailable(Exception):
    pass


@backoff.on_exception(backoff.expo,
                      EsNotAvailable,max_time=60)
async def wait_for_es():
    while True:
        es = AsyncElasticsearch(hosts=[f'{ES_HOST}:{ES_PORT}'])

        if await es.ping():
            break
        else:
            raise EsNotAvailable


if '__name__'=='__main__':
    asyncio.run(wait_for_es())
