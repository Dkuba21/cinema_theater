"""Ожидание готовности сервиса Redis."""
import asyncio

import aioredis
import backoff

from tests.functional.settings import REDIS_HOST


class RedisNotAvailable(Exception):
    pass


@backoff.on_exception(backoff.expo,
                      RedisNotAvailable,max_time=60)
async def wait_for_redis():
    while True:
        redis = aioredis.from_url(REDIS_HOST, encoding="utf-8", decode_responses=True)

        if await redis.ping():
            break
        else:
            raise RedisNotAvailable


if '__name__'=='__main__':
    asyncio.run(wait_for_redis())
